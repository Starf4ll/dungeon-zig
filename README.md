# Dungeon Zig

Dungeon Zig is an application designed to assist with automation in Dungeon Siege mod making through the use of scripts. It supports reading and writing of Gas files, in addition to a basic CSV reader.

A script to generate monsters with stats based on predefined formulas and multipliers from a CSV is included. For details, refer to the script-specific readme under `/scripts/generate_monsters/`.

## Installation

Clone this repository to your machine using git. Alternatively you may download it as a zip and extract the files.

```bash
git clone https://gitlab.com/Starf4ll/dungeon-zig.git
```

## Usage

If customization is not desired, then simply run the pre-built `dungeon_zig.exe` in the main directory.

Otherwise, if you would like to customize the formulas for an existing script, modify script logic or add new scripts, you will need to download and install Zig, and re-build the application.

**NOTE**: Dungeon Zig was built and tested with the Release version `0.12.0` of Zig. The `master` version may not work correctly.

1. Download Zig from https://ziglang.org/download/
2. Extract the archive anywhere on your machine
3. Add the directory of the extracted files to your PATH
4. Open a terminal in the directory you cloned this repository to
5. Run `zig build run`

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/)

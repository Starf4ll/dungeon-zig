const std = @import("std");
const ArrayList = std.ArrayList;
const Allocator = std.mem.Allocator;

const VERSION: u8 = 1;

pub const CsvParseError = error{
    NullCharacter,
    MismatchedHeaderRowLengths,
};

pub const CsvParser = struct {
    /// Input File Buffer
    var buffer: []u8 = undefined;
    /// Current character index of File Buffer
    var i: u32 = undefined;
    /// Temporary storage for CSV Headers
    var headers: ArrayList([]const u8) = undefined;
    /// Temporary storage for CSV Rows
    var rows: ArrayList([][]const u8) = undefined;

    /// Parses a CSV file at the given path
    pub fn parse(allocator: Allocator, file_path: []const u8, comptime delimiter: u8) !*Csv {
        var cwd = std.fs.cwd();
        const csv_file = try cwd.openFile(file_path, .{});

        buffer = try csv_file.readToEndAlloc(allocator, std.math.maxInt(u32));
        i = 0;
        headers = ArrayList([]const u8).init(allocator);
        rows = ArrayList([][]const u8).init(allocator);

        defer allocator.free(buffer);
        errdefer headers.deinit();
        errdefer rows.deinit();

        try parseHeaders(allocator, delimiter);
        try parseRows(allocator, delimiter);

        return try Csv.init(allocator, file_path, delimiter, try headers.toOwnedSlice(), try rows.toOwnedSlice());
    }

    /// Parses the headers of a CSV file
    fn parseHeaders(allocator: Allocator, comptime delimiter: u8) !void {
        var cell_start = i;
        while (i < buffer.len) : (i += 1) {
            switch (buffer[i]) {
                0x00 => return CsvParseError.NullCharacter,
                delimiter => try addCell(allocator, &headers, &cell_start),
                '\n' => {
                    try addCell(allocator, &headers, &cell_start);
                    i += 1;
                    return;
                },
                else => continue,
            }
        }
        return;
    }

    /// Parses the rows of a CSV file
    fn parseRows(allocator: Allocator, comptime delimiter: u8) !void {
        var row = try ArrayList([]const u8).initCapacity(allocator, headers.items.len);
        var cell_start = i;
        errdefer row.deinit();
        while (i < buffer.len) : (i += 1) {
            switch (buffer[i]) {
                0x00 => return CsvParseError.NullCharacter,
                delimiter => try addCell(allocator, &row, &cell_start),
                '\n' => {
                    try addCell(allocator, &row, &cell_start);
                    try addRow(&row);
                    continue;
                },
                else => continue,
            }
        }
        try addCell(allocator, &row, &cell_start);
        try addRow(&row);
        return;
    }

    /// Adds a CSV cell into the temporary storage for either a row or headers
    fn addCell(allocator: Allocator, row_or_header: *ArrayList([]const u8), cell_start: *u32) !void {
        const ptr = try row_or_header.addOne();
        if (buffer[i - 1] == '\r') {
            ptr.* = try Allocator.dupe(allocator, u8, buffer[cell_start.* .. i - 1]);
        } else {
            ptr.* = try Allocator.dupe(allocator, u8, buffer[cell_start.*..i]);
        }
        cell_start.* = i + 1;
    }

    /// Adds a CSV row into the temporary storage for rows
    /// Skips rows if they're completely empty
    /// Raises an error if the number of cells in a row does not match the number of headers
    fn addRow(row: *ArrayList([]const u8)) !void {
        if (row.items.len > 0) blk: {
            for (row.items) |*cell| {
                if (cell.len != 0) break :blk;
            }
            row.clearAndFree();
            return;
        }
        if (row.items.len != headers.items.len) {
            std.log.err("Found {d} columns for row at position {d}, but expected {d}!", .{ row.items.len, i, headers.items.len });
            return CsvParseError.MismatchedHeaderRowLengths;
        }
        try rows.append(try row.toOwnedSlice());
    }
};

/// Parsed CSV with headers and rows
pub const Csv = struct {
    const Self = @This();

    allocator: Allocator,
    file_path: []const u8,
    delimiter: u8,
    headers: [][]const u8,
    rows: [][][]const u8,

    /// Initializes a new CSV File
    /// Deinitialize with deinit()
    pub fn init(allocator: Allocator, _file_path: []const u8, delimiter: u8, headers: [][]const u8, rows: [][][]const u8) !*Self {
        const self = try allocator.create(Csv);
        const file_path = try Allocator.dupe(allocator, u8, _file_path);
        self.* = .{
            .allocator = allocator,
            .file_path = file_path,
            .delimiter = delimiter,
            .headers = headers,
            .rows = rows,
        };
        return self;
    }

    /// Frees all memory used by this CSV
    pub fn deinit(self: *Self) void {
        self.allocator.free(self.file_path);
        self.allocator.free(self.headers);
        self.allocator.free(self.rows);
        self.allocator.destroy(self);
    }

    /// Returns the index for a given column name, if it exists
    pub fn getColumnIndex(self: *Self, column_name: []const u8) ?usize {
        for (self.headers, 0..) |*header, i| {
            if (std.mem.eql(u8, header.*, column_name)) return i;
        }
        return null;
    }

    /// Reduces CSV rows to a single column, if the column name exists
    pub fn getColumn(self: *Self, column_name: []const u8) !?[][]const u8 {
        if (self.getColumnIndex(column_name)) |col_index| {
            var column = try ArrayList([]const u8).initCapacity(self.allocator, self.rows.len);
            for (self.rows) |row| {
                try column.append(row[col_index]);
            }
            return try column.toOwnedSlice();
        }
        return null;
    }

    /// Returns the row at the given index, if the row index is within range
    pub fn getRow(self: *Self, row_index: usize) ?[][]const u8 {
        if (row_index >= self.rows.len) return null;
        return self.rows[row_index];
    }

    /// Gets the value of a cell for the given column at the given row index, if the column name exists and the row index is within range
    pub fn getCell(self: *Self, column_name: []const u8, row_index: usize) ?[]const u8 {
        if (row_index >= self.rows.len) return null;
        if (self.getColumnIndex(column_name)) |col_index| {
            return self.rows[row_index][col_index];
        }
        return null;
    }

    // Prints the Csv to the debug log
    pub fn debugPrint(self: *Self) void {
        std.debug.print("file_path: {s} | delimiter: {c}\n", .{ self.file_path, self.delimiter });
        for (self.headers) |*header| {
            std.debug.print("{s},", .{header.*});
        }
        std.debug.print("\n", .{});
        for (self.rows) |*row| {
            for (row.*) |*cell| {
                std.debug.print("{s},", .{cell.*});
            }
            std.debug.print("\n", .{});
        }
    }
};

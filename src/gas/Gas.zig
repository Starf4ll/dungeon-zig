const std = @import("std");
const testing = std.testing;
const ArrayList = std.ArrayList;
const DoublyLinkedList = std.DoublyLinkedList;
const Allocator = std.mem.Allocator;

const VERSION: u8 = 1;

pub const GasEntryNode = DoublyLinkedList(GasEntry).Node;
pub const ComponentNode = DoublyLinkedList(Component).Node;
pub const AttributeNode = DoublyLinkedList(Attribute).Node;

pub const GasFile = struct {
    const Self = @This();

    allocator: Allocator,
    entries: DoublyLinkedList(GasEntry),
    file_path: []const u8,
    file_name: []const u8,

    /// Initializes existing memory space with an empty GasFile
    /// Deinitialize with deinit()
    pub fn init(allocator: Allocator, ptr: *GasFile, _file_path: []const u8, _file_name: []const u8) !*Self {
        const file_path = try Allocator.dupe(allocator, u8, _file_path);
        const file_name = try Allocator.dupe(allocator, u8, _file_name);
        ptr.* = .{
            .allocator = allocator,
            .entries = DoublyLinkedList(GasEntry){},
            .file_path = file_path,
            .file_name = file_name,
        };
        return ptr;
    }

    /// Initializes an empty GasFile by reserving new memory space
    /// Deinitialize with deinitManaged()
    pub fn initManaged(allocator: Allocator, file_path: []const u8, file_name: []const u8) !*Self {
        const self = try allocator.create(GasFile);
        return init(allocator, self, file_path, file_name);
    }

    /// Recursively frees all memory used by GasEntries and their child components
    pub fn deinit(self: *Self) void {
        var i: u16 = 0;
        const iend = self.entries.len;
        while (i < iend) : (i += 1) {
            const node = self.entries.pop().?;
            node.data.deinit();
            self.allocator.destroy(node);
        }
        self.allocator.free(self.file_path);
        self.allocator.free(self.file_name);
    }

    /// Recursively frees all memory used by GasEntries and their child components, then clears the reserved memory space for this GasFile
    pub fn deinitManaged(self: *Self) void {
        self.deinit();
        self.allocator.destroy(self);
    }

    /// Validates that the GasFile contains no child GasEntries
    pub fn isEmpty(self: *Self) bool {
        return self.entries.len == 0;
    }

    /// Returns the first GasEntry which matches the given name and type, if it exists, otherwise return null
    pub fn getEntry(self: *Self, name: []const u8, gas_type: []const u8) ?*GasEntry {
        var it = self.entries.first;
        while (it) |node| : (it = node.next) {
            if (std.mem.eql(u8, node.data.name, name) and std.mem.eql(u8, node.data.gas_type, gas_type)) {
                return &node.data;
            }
        }
        return null;
    }

    /// Inserts a new GasEntry with the given name and type
    pub fn addEntry(self: *Self, name: []const u8, gas_type: []const u8) !*GasEntry {
        const node = try self.allocator.create(GasEntryNode);
        node.* = .{ .data = try GasEntry.init(self.allocator, self, name, gas_type) };
        self.entries.append(node);
        return &self.entries.last.?.data;
    }

    /// Removes the first GasEntry found with the given name and type. Does not search recursively.
    pub fn removeEntry(self: *Self, name: []const u8, gas_type: []const u8) void {
        var it = self.entries.first;
        while (it) |node| : (it = node.next) {
            if (std.mem.eql(u8, node.data.name, name) and std.mem.eql(u8, node.data.gas_type, gas_type)) {
                node.data.deinit();
                self.entries.remove(node);
                self.allocator.destroy(node);
                return;
            }
        }
    }

    /// Removes the given GasEntry by pointer. Does not search recursively.
    pub fn removeEntryPtr(self: *Self, entry: *GasEntry) void {
        var it = self.entries.first;
        while (it) |node| : (it = node.next) {
            if (&node.data == entry) {
                node.data.deinit();
                self.entries.remove(node);
                self.allocator.destroy(node);
                return;
            }
        }
    }

    /// Return the most recently appended GasEntry in this file, or null if the entry list is empty
    pub fn peekEntries(self: *Self) ?*GasEntry {
        if (self.isEmpty()) return null;
        return &self.entries.last.?.data;
    }

    // Prints this GasFile to the debug log
    pub fn debugPrint(self: *Self) !void {
        std.debug.print("GasFile | {*}, file_path: \"{s}\", file_name: \"{s}\"\n", .{ self, self.file_path, self.file_name });
        if (self.isEmpty()) return;

        var indent: u8 = 2;
        var e = self.entries.first;
        while (e) |e_node| : (e = e_node.next) {
            try e_node.data.debugPrint(&indent);
        }

        std.debug.print("\n---------------------------\n\n", .{});
    }
};

pub const GasEntry = struct {
    const Self = @This();

    allocator: Allocator,
    file: *GasFile,
    name: []const u8,
    gas_type: []const u8,
    attributes: DoublyLinkedList(Attribute),
    components: DoublyLinkedList(Component),

    /// Initializes an empty GasEntry with a given name and type
    /// Deinitialize with deinit()
    pub fn init(allocator: Allocator, file: *GasFile, _name: []const u8, _gas_type: []const u8) !Self {
        const name = try Allocator.dupe(allocator, u8, _name);
        const gas_type = try Allocator.dupe(allocator, u8, _gas_type);
        return .{
            .allocator = allocator,
            .file = file,
            .name = name,
            .gas_type = gas_type,
            .attributes = DoublyLinkedList(Attribute){},
            .components = DoublyLinkedList(Component){},
        };
    }

    /// Recursively frees all memory used by attributes and components, including child components
    fn deinit(self: *Self) void {
        var i: u16 = 0;
        var iend = self.attributes.len;
        while (i < iend) : (i += 1) {
            const node = self.attributes.pop().?;
            node.data.deinit();
            self.allocator.destroy(node);
        }
        i = 0;
        iend = self.components.len;
        while (i < iend) : (i += 1) {
            const node = self.components.pop().?;
            node.data.deinit();
            self.allocator.destroy(node);
        }
        self.allocator.free(self.name);
        self.allocator.free(self.gas_type);
    }

    /// Validates that the GasEntry contains no attributes
    pub fn isAttributesEmpty(self: *Self) bool {
        return self.attributes.len == 0;
    }

    /// Validates that the GasEntry contains no components
    pub fn isComponentsEmpty(self: *Self) bool {
        return self.components.len == 0;
    }

    /// Validates that the GasEntry contains no attributes or child components
    pub fn isEmpty(self: *Self) bool {
        return self.isAttributesEmpty() and self.isComponentsEmpty();
    }

    /// Returns the first Attribute which matches the given name, if it exists, otherwise return null
    pub fn getAttribute(self: *Self, name: []const u8) ?*Attribute {
        var it = self.attributes.first;
        while (it) |node| : (it = node.next) {
            if (std.mem.eql(u8, node.data.name, name)) {
                return &node.data;
            }
        }
        return null;
    }

    /// Inserts a new Attribute with the given name and value
    pub fn addAttribute(self: *Self, name: []const u8, value: []const u8) !*Attribute {
        const node = try self.allocator.create(AttributeNode);
        node.* = .{ .data = try Attribute.init(self.allocator, name, value) };
        self.attributes.append(node);
        return &self.attributes.last.?.data;
    }

    /// Return the most recently appended Attribute in this GasEntry, or null if the attribute list is empty
    pub fn peekAttributes(self: *Self) ?*Attribute {
        if (self.isAttributesEmpty()) return null;
        return &self.attributes.last.?.data;
    }

    /// Returns the first Component which matches the given name, if it exists, otherwise return null
    pub fn getComponent(self: *Self, name: []const u8) ?*Component {
        var it = self.components.first;
        while (it) |node| : (it = node.next) {
            if (std.mem.eql(u8, node.data.name, name)) {
                return &node.data;
            }
        }
        return null;
    }

    /// Inserts a new Component with the given name
    pub fn addComponent(self: *Self, name: []const u8) !*Component {
        const node = try self.allocator.create(ComponentNode);
        node.* = .{ .data = try Component.init(self.allocator, self, null, name) };
        self.components.append(node);
        return &self.components.last.?.data;
    }

    /// Removes the first Component found with the given name. Does not search recursively.
    pub fn removeComponent(self: *Self, name: []const u8) void {
        var it = self.components.first;
        while (it) |node| : (it = node.next) {
            if (std.mem.eql(u8, node.data.name, name)) {
                node.data.deinit();
                self.components.remove(node);
                self.allocator.destroy(node);
                return;
            }
        }
    }

    /// Removes the given Component by pointer. Does not search recursively.
    pub fn removeComponentPtr(self: *Self, component: *Component) void {
        var it = self.components.first;
        while (it) |node| : (it = node.next) {
            if (&node.data == component) {
                node.data.deinit();
                self.components.remove(node);
                self.allocator.destroy(node);
                return;
            }
        }
    }

    /// Return the most recently appended Component in this GasEntry, or null if the component list is empty
    pub fn peekComponents(self: *Self) ?*Component {
        if (self.isComponentsEmpty()) return null;
        return &self.components.last.?.data;
    }

    /// Clones this GasEntry into a GasFile
    /// new_gas_file: Pointer to the GasFile the cloned entry should belong to, or null if it will belong to the current file
    pub fn clone(self: *Self, new_gas_file: ?*GasFile) !*GasEntry {
        var file = new_gas_file orelse self.file;
        var new_entry = try file.addEntry(self.name, self.gas_type);

        var a = self.attributes.first;
        while (a) |a_node| : (a = a_node.next) {
            _ = try new_entry.addAttribute(a_node.data.name, a_node.data.value);
        }

        var c = self.components.first;
        while (c) |c_node| : (c = c_node.next) {
            _ = try c_node.data.clone(new_entry, null);
        }

        return new_entry;
    }

    /// Copies all Attributes and Components (recursive) from this GasEntry into another GasEntry
    /// Attributes with matching names will be ignored, unless overwrite is passed as true, in which case they are overwritten
    pub fn merge(self: *Self, destination: *GasEntry, overwrite: bool) !void {
        if (self.isEmpty()) return;

        var s_a = self.attributes.first;
        while (s_a) |s_a_node| : (s_a = s_a_node.next) {
            var found: bool = false;
            var d_a = destination.attributes.first;
            while (d_a) |d_a_node| : (d_a = d_a_node.next) {
                if (std.mem.eql(u8, s_a_node.data.name, d_a_node.data.name)) {
                    found = true;
                    if (overwrite) try d_a_node.data.update(s_a_node.data.value);
                }
            }
            if (!found) {
                _ = try destination.addAttribute(s_a_node.data.name, s_a_node.data.value);
            }
        }

        var s_c = self.components.first;
        while (s_c) |s_c_node| : (s_c = s_c_node.next) {
            var found: bool = false;
            var d_c = destination.components.first;
            while (d_c) |d_c_node| : (d_c = d_c_node.next) {
                if (std.mem.eql(u8, s_c_node.data.name, d_c_node.data.name)) {
                    found = true;
                    try s_c_node.data.merge(&d_c_node.data, overwrite);
                }
            }
            if (!found) {
                _ = try s_c_node.data.clone(destination, null);
            }
        }
    }

    // Prints this GasEntry to the debug log
    pub fn debugPrint(self: *Self, indent: *u8) !void {
        const spacing = try debugIndent(self.allocator, indent.*);
        defer self.allocator.free(spacing);
        std.debug.print("{s}Entry | {*}, name: \"{s}\", gas_type: \"{s}\", file: \"{*}\"\n", .{
            spacing,
            self,
            self.name,
            self.gas_type,
            self.file,
        });

        if (self.isEmpty()) return;
        indent.* += 2;
        defer indent.* -= 2;

        var a = self.attributes.first;
        while (a) |a_node| : (a = a_node.next) {
            try a_node.data.debugPrint(indent);
        }

        var c = self.components.first;
        while (c) |c_node| : (c = c_node.next) {
            try c_node.data.debugPrint(indent);
        }
    }
};

pub const Component = struct {
    const Self = @This();

    allocator: Allocator,
    gas_entry: *GasEntry,
    parent: ?*const Component,
    name: []const u8,
    attributes: DoublyLinkedList(Attribute),
    components: DoublyLinkedList(Component),

    /// Initializes an empty Component with a given parent GasEntry, optional parent Component and name
    /// Deinitialize with deinit()
    pub fn init(allocator: Allocator, gas_entry: *GasEntry, parent: ?*const Component, _name: []const u8) !Self {
        const name = try Allocator.dupe(allocator, u8, _name);
        return .{
            .allocator = allocator,
            .gas_entry = gas_entry,
            .parent = parent,
            .name = name,
            .attributes = DoublyLinkedList(Attribute){},
            .components = DoublyLinkedList(Component){},
        };
    }

    /// Recursively frees all memory used by attributes and components, including child components
    fn deinit(self: *Self) void {
        var i: u16 = 0;
        var iend = self.attributes.len;
        while (i < iend) : (i += 1) {
            const node = self.attributes.pop().?;
            node.data.deinit();
            self.allocator.destroy(node);
        }
        i = 0;
        iend = self.components.len;
        while (i < iend) : (i += 1) {
            const node = self.components.pop().?;
            node.data.deinit();
            self.allocator.destroy(node);
        }
        self.allocator.free(self.name);
    }

    /// Validates that the Component contains no attributes
    pub fn isAttributesEmpty(self: *Self) bool {
        return self.attributes.len == 0;
    }

    /// Validates that the Component contains no child components
    pub fn isComponentsEmpty(self: *Self) bool {
        return self.components.len == 0;
    }

    /// Validates that the Component contains no attributes or child components
    pub fn isEmpty(self: *Self) bool {
        return self.isAttributesEmpty() and self.isComponentsEmpty();
    }

    /// Returns the first Attribute which matches the given name, if it exists, otherwise return null
    pub fn getAttribute(self: *Self, name: []const u8) ?*Attribute {
        var it = self.attributes.first;
        while (it) |node| : (it = node.next) {
            if (std.mem.eql(u8, node.data.name, name)) {
                return &node.data;
            }
        }
        return null;
    }

    /// Inserts a new Attribute with the given name and value
    pub fn addAttribute(self: *Self, name: []const u8, value: []const u8) !*Attribute {
        const node = try self.allocator.create(AttributeNode);
        node.* = .{ .data = try Attribute.init(self.allocator, name, value) };
        self.attributes.append(node);
        return &self.attributes.last.?.data;
    }

    /// Return the most recently appended Attribute in this Component, or null if the attribute list is empty
    pub fn peekAttributes(self: *Self) ?*Attribute {
        if (self.isAttributesEmpty()) return null;
        return &self.attributes.last.?.data;
    }

    /// Returns the first Component which matches the given name, if it exists, otherwise return null
    pub fn getComponent(self: *Self, name: []const u8) ?*Component {
        var it = self.components.first;
        while (it) |node| : (it = node.next) {
            if (std.mem.eql(u8, node.data.name, name)) {
                return &node.data;
            }
        }
        return null;
    }

    /// Inserts a new Component with the given name
    pub fn addComponent(self: *Self, name: []const u8) !*Component {
        const node = try self.allocator.create(ComponentNode);
        node.* = .{ .data = try Component.init(self.allocator, self.gas_entry, self, name) };
        self.components.append(node);
        return &self.components.last.?.data;
    }

    /// Removes the first Component found with the given name. Does not search recursively.
    pub fn removeComponent(self: *Self, name: []const u8) void {
        var it = self.components.first;
        while (it) |node| : (it = node.next) {
            if (std.mem.eql(u8, node.data.name, name)) {
                node.data.deinit();
                self.components.remove(node);
                self.allocator.destroy(node);
                return;
            }
        }
    }

    /// Removes the given Component by pointer. Does not search recursively.
    pub fn removeComponentPtr(self: *Self, component: *Component) void {
        var it = self.components.first;
        while (it) |node| : (it = node.next) {
            if (&node.data == component) {
                node.data.deinit();
                self.components.remove(node);
                self.allocator.destroy(node);
                return;
            }
        }
    }

    /// Return the most recently appended Component in this Component, or null if the component list is empty
    pub fn peekComponents(self: *Self) ?*Component {
        if (self.isComponentsEmpty()) return null;
        return &self.components.last.?.data;
    }

    /// Clones this Component into a GasEntry or parent Component
    /// new_entry: Pointer to the GasEntry the cloned component should belong to, or null if it will belong to the current entry
    ///            If new_parent is provided, the parent's GasEntry will be used instead of new_entry
    /// new_parent: Pointer to the Component the cloned component should belong to, or null if it has no parent
    pub fn clone(self: *Self, new_entry: ?*GasEntry, parent: ?*Component) !*Component {
        const entry = new_entry orelse self.gas_entry;
        const new_component: *Component = if (parent) |p| try p.addComponent(self.name) else try entry.addComponent(self.name);

        var a = self.attributes.first;
        while (a) |a_node| : (a = a_node.next) {
            _ = try new_component.addAttribute(a_node.data.name, a_node.data.value);
        }

        var c = self.components.first;
        while (c) |c_node| : (c = c_node.next) {
            _ = try c_node.data.clone(new_entry, new_component);
        }

        return new_component;
    }

    /// Copies all Attributes and Components (recursive) from this Component into another Component
    /// Attributes with matching names will be ignored, unless overwrite is passed as true, in which case they are overwritten
    pub fn merge(self: *Self, destination: *Component, overwrite: bool) !void {
        if (self.isEmpty()) return;

        var s_a = self.attributes.first;
        while (s_a) |s_a_node| : (s_a = s_a_node.next) {
            var found: bool = false;
            var d_a = destination.attributes.first;
            while (d_a) |d_a_node| : (d_a = d_a_node.next) {
                if (std.mem.eql(u8, s_a_node.data.name, d_a_node.data.name)) {
                    found = true;
                    if (overwrite) try d_a_node.data.update(s_a_node.data.value);
                }
            }
            if (!found) {
                _ = try destination.addAttribute(s_a_node.data.name, s_a_node.data.value);
            }
        }

        var s_c = self.components.first;
        while (s_c) |s_c_node| : (s_c = s_c_node.next) {
            var found: bool = false;
            var d_c = destination.components.first;
            while (d_c) |d_c_node| : (d_c = d_c_node.next) {
                if (std.mem.eql(u8, s_c_node.data.name, d_c_node.data.name)) {
                    found = true;
                    try s_c_node.data.merge(&d_c_node.data, overwrite);
                }
            }
            if (!found) {
                _ = try s_c_node.data.clone(destination.gas_entry, destination);
            }
        }
    }

    // Prints this Component to the debug log
    pub fn debugPrint(self: *Self, indent: *u8) !void {
        const parent_str: []const u8 = blk: {
            if (self.parent) |parent| {
                break :blk parent.name;
            }
            break :blk "";
        };

        const spacing = try debugIndent(self.allocator, indent.*);
        defer self.allocator.free(spacing);
        std.debug.print("{s}Component | {*}, name: \"{s}\", entry: \"{s}:{*}\", parent: \"{s}:{*}\"\n", .{
            spacing,
            self,
            self.name,
            self.gas_entry.name,
            self.gas_entry,
            parent_str,
            self.parent,
        });

        if (self.isEmpty()) return;
        indent.* += 2;
        defer indent.* -= 2;

        var a = self.attributes.first;
        while (a) |a_node| : (a = a_node.next) {
            try a_node.data.debugPrint(indent);
        }

        var c = self.components.first;
        while (c) |c_node| : (c = c_node.next) {
            try c_node.data.debugPrint(indent);
        }
    }
};

pub const Attribute = struct {
    const Self = @This();

    allocator: Allocator,
    name: []const u8,
    value: []const u8,

    /// Initializes an attribute with a given name and value
    /// Deinitialize with deinit()
    pub fn init(allocator: Allocator, _name: []const u8, _value: []const u8) !Self {
        const name = try Allocator.dupe(allocator, u8, _name);
        const value = try Allocator.dupe(allocator, u8, _value);
        return .{
            .allocator = allocator,
            .name = name,
            .value = value,
        };
    }

    /// Frees all memory used by name and value
    fn deinit(self: Self) void {
        self.allocator.free(self.name);
        self.allocator.free(self.value);
    }

    /// Updates the value of this Attribute
    pub fn update(self: *Self, _new_value: []const u8) !void {
        self.allocator.free(self.value);
        const new_value = try Allocator.dupe(self.allocator, u8, _new_value);
        self.value = new_value;
    }

    // Prints this Attribute to the debug log
    pub fn debugPrint(self: *Self, indent: *u8) !void {
        const spacing = try debugIndent(self.allocator, indent.*);
        defer self.allocator.free(spacing);
        std.debug.print("{s}Attribute | name: \"{s}\", value: \"{s}\"\n", .{ spacing, self.name, self.value });
    }
};

// Returns a slice of whitespace of the given size
fn debugIndent(allocator: Allocator, size: u16) ![]const u8 {
    var spacing = try std.ArrayList(u8).initCapacity(allocator, 32);
    try spacing.appendNTimes(' ', size);
    return spacing.toOwnedSlice();
}

test "Gas Integration Test" {
    const allocator = testing.allocator;

    var test_file = try GasFile.initManaged(allocator, "/test/", "test_file.gas");
    var test_file_2 = try GasFile.initManaged(allocator, "/test/", "test_file_2.gas");
    defer test_file.deinitManaged();
    defer test_file_2.deinitManaged();

    // GasFile | test_file
    //   Entry | name: "test_template", gas_type: "template"
    //     Attribute | name: "attr0", value: "value0"
    //     Attribute | name: "attr1", value: "badvalue"
    //     Component | name: "test_component", entry: "test_template", parent: ""
    //       Attribute | name: "attr2", value: "badvalue"
    //       Component | name: "test_component_2", entry: "test_template", parent: "test_component"
    //         Attribute | name: "attr3", value: "badvalue"
    const test_entry = try test_file.addEntry("test_template", "template");
    _ = try test_entry.addAttribute("attr0", "value0");
    _ = try test_entry.addAttribute("attr1", "badvalue");
    const test_component = try test_entry.addComponent("test_component");
    _ = try test_component.addAttribute("attr2", "badvalue");
    const test_component_2 = try test_component.addComponent("test_component_2");
    _ = try test_component_2.addAttribute("attr3", "badvalue");

    // GasFile | test_file_2
    //   Entry | name: "test_template", gas_type: "template"
    //     Attribute | name: "attr1", value: "value1"
    //     Component | name: "test_component", entry: "test_template", parent: ""
    //       Attribute | name: "attr2", value: "value2"
    //       Component | name: "test_component_2", entry: "test_template", parent: "test_component"
    //         Attribute | name: "attr3", value: "value3"
    const f2_test_entry = try test_file_2.addEntry("test_template", "template");
    _ = try f2_test_entry.addAttribute("attr1", "value1");
    const f2_test_component = try f2_test_entry.addComponent("test_component");
    _ = try f2_test_component.addAttribute("attr2", "value2");
    const f2_test_component_2 = try f2_test_component.addComponent("test_component_2");
    _ = try f2_test_component_2.addAttribute("attr3", "value3");

    // GasFile | test_file
    //   Entry | name: "test_template", gas_type: "template"
    //     ...
    //   Entry | name: "test_template_2", gas_type: "template"
    //     Attribute | name: "attr4", value: "value4"
    //     Component | name: "f2_e2_test_component", entry: "test_template_2", parent: ""
    //       Attribute | name: "attr5", value: "value5"
    const test_entry_2 = try test_file.addEntry("test_template_2", "template");
    _ = try test_entry_2.addAttribute("attr4", "badvalue");

    // GasFile | test_file_2
    //   Entry | name: "test_template", gas_type: "template"
    //     ...
    //   Entry | name: "test_template_2", gas_type: "template"
    //     Attribute | name: "attr4", value: "value4"
    //     Component | name: "f2_e2_test_component", entry: "test_template_2", parent: ""
    //       Attribute | name: "attr5", value: "value5"
    const f2_test_entry_2 = try test_file_2.addEntry("test_template_2", "template");
    _ = try f2_test_entry_2.addAttribute("attr4", "value4");
    const f2_e2_test_component = try f2_test_entry_2.addComponent("f2_e2_test_component");
    _ = try f2_e2_test_component.addAttribute("attr5", "value5");

    // Test GasEntry & Component merge overwrite=true behavior
    try f2_test_entry.merge(test_entry, true);
    try testing.expectEqualStrings("value1", test_entry.getAttribute("attr1").?.value);
    try testing.expectEqualStrings("value2", test_component.getAttribute("attr2").?.value);
    try testing.expectEqualStrings("value3", test_component_2.getAttribute("attr3").?.value);

    // Test GasEntry & Component merge overwrite=false behavior
    try f2_test_entry_2.merge(test_entry_2, false);
    try testing.expectEqualStrings("badvalue", test_entry_2.getAttribute("attr4").?.value);
    try testing.expectEqualStrings("value5", test_entry_2.getComponent("f2_e2_test_component").?.getAttribute("attr5").?.value);

    // Test GasEntry clone behavior
    try testing.expect(test_file_2.entries.len == 2);
    _ = try test_entry_2.clone(test_file_2);
    try testing.expect(test_file_2.entries.len == 3);

    // Test GasEntry remove behavior
    test_file.removeEntry("test_template_2", "template");
    try testing.expect(test_file.entries.len == 1);
    try testing.expectEqual(test_entry, test_file.peekEntries().?);

    // Test Component remove behavior
    test_entry.removeComponent("test_component");
    try testing.expect(test_entry.components.len == 0);
}

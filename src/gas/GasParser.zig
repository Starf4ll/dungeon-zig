const std = @import("std");
const SegmentedList = std.SegmentedList;
const Allocator = std.mem.Allocator;
const File = std.fs.File;
const Timer = std.time.Timer;

const Gas = @import("Gas.zig");

const VERSION: u8 = 1;

pub const GasParseError = error{
    NullCharacter,
    InvalidCharacter,
    UnclosedBracing,
    UnexpectedEOF,
};

/// Crawls a given directory (local to the current working directory) and all subdirectories, parsing Dungeon Siege Gas files
/// Returns files as an SegmentedList of GasFile structs
pub fn parseDirectory(allocator: Allocator, dir: []const u8) !SegmentedList(Gas.GasFile, 32) {
    var total_timer = try Timer.start();

    var gas_files = SegmentedList(Gas.GasFile, 32){};

    var cwd = std.fs.cwd();
    var input_dir = try cwd.openDir(dir, .{ .iterate = true });
    var walker: std.fs.Dir.Walker = try input_dir.walk(allocator);
    defer input_dir.close();
    defer walker.deinit();

    while (try walker.next()) |file_entry| {
        if (file_entry.kind == File.Kind.directory) {
            std.log.info("Scanning {s}", .{file_entry.path});
            continue;
        }
        if (file_entry.kind != File.Kind.file) continue;
        if (file_entry.basename.len < 3) continue;
        if (!std.mem.eql(u8, file_entry.basename[file_entry.basename.len - 3 ..], "gas")) continue;

        const in_file = try input_dir.openFile(file_entry.path, .{});
        const gas_file = try gas_files.addOne(allocator);
        const file_path = std.fs.path.dirname(file_entry.path).?;
        _ = try Gas.GasFile.init(allocator, gas_file, file_path, file_entry.basename);
        _ = try parse(allocator, in_file, file_path, file_entry.basename, gas_file);

        defer in_file.close();
    }

    std.log.info("Total Time Taken To Parse: {d:.4}ms", .{
        @as(f128, @floatFromInt(total_timer.lap())) / @as(f32, @floatFromInt(std.time.ns_per_ms)),
    });

    return gas_files;
}

/// Parses a Dungeon Siege Gas file at the given absolute file path into a GasFile struct
/// An empty GasFile is returned instead if an error occurs during parsing
/// Caller owns the memory for the GasFile and must call deinitManaged() instead of deinit() to deinitialize it
pub fn parseFile(allocator: Allocator, abs_file_path: []const u8) !*Gas.GasFile {
    const file = try std.fs.openFileAbsolute(abs_file_path, .{});
    const file_path = std.fs.path.dirname(abs_file_path).?;
    const file_name = std.fs.path.basename(abs_file_path);
    const gas_file = try Gas.GasFile.initManaged(allocator, file_path, file_name);
    return try parse(allocator, file, file_path, file_name, gas_file);
}

/// Parses a Dungeon Siege Gas file into a GasFile struct
/// An empty GasFile is returned instead if an error occurs during parsing
fn parse(allocator: Allocator, file: File, file_path: []const u8, file_name: []const u8, gas_file: *Gas.GasFile) !*Gas.GasFile {
    std.log.info("  Parsing {s}", .{file_name});
    var file_timer = try Timer.start();

    const buffer = try file.readToEndAlloc(allocator, std.math.maxInt(u24));
    var i: u24 = 0;
    defer allocator.free(buffer);

    GasParser.parseFile(&buffer, &i, gas_file) catch |err| {
        switch (err) {
            GasParseError.InvalidCharacter => std.log.err("Error parsing {s}: {} at position {d}, found char '{c}'", .{ file_name, err, i, buffer[i] }),
            else => std.log.err("Error parsing {s}: {} at position {d}", .{ file_name, err, i }),
        }
        if (@errorReturnTrace()) |trace| {
            std.debug.dumpStackTrace(trace.*);
        }
        gas_file.deinit();
        return try Gas.GasFile.init(allocator, gas_file, file_path, file_name);
    };

    std.debug.print("  Time Taken To Parse: {d:.4}ms\n", .{@as(f128, @floatFromInt(file_timer.lap())) / @as(f64, @floatFromInt(std.time.ns_per_ms))});
    return gas_file;
}

/// Internal handler for Gas File parsing
const GasParser = struct {
    /// Input File Buffer
    var buffer: *const []u8 = undefined;
    /// Current character index of File Buffer
    var i: *u24 = undefined;
    /// Output Gas File
    var gas_file: *Gas.GasFile = undefined;

    /// Outermost layer of Gas File
    /// Parses empty space, until a Comment or GasEntry is found
    ///
    ///                                        <-- You are here
    /// /*Block Comment*/    // Line Comment
    /// [t:template,n:my_template]
    /// {
    ///     ...
    /// }
    fn parseFile(_buffer: *const []u8, _i: *u24, _gas_file: *Gas.GasFile) !void {
        buffer = _buffer;
        i = _i;
        gas_file = _gas_file;
        while (i.* < buffer.len) : (i.* += 1) {
            switch (buffer.*[i.*]) {
                0x00 => return GasParseError.NullCharacter,
                '\t', '\n', '\r', ' ' => continue,
                '/' => try parseStartComment(),
                '[' => try parseFileTxGasEntry(),
                else => return GasParseError.InvalidCharacter,
            }
        }
        return;
    }

    /// Parses whether the forward slash found in the previous method turns into a Line Comment or Block Comment
    ///
    ///  -- You are here      -- Or here
    ///  V                    V
    /// /*Block Comment*/    // Line Comment
    /// [t:template,n:my_template]
    /// {
    ///     ...
    /// }
    fn parseStartComment() GasParseError!void {
        i.* += 1;
        if (i.* < buffer.len) {
            switch (buffer.*[i.*]) {
                0x00 => return GasParseError.NullCharacter,
                '/' => try parseComment(),
                '*' => try parseCommentBlock(),
                else => return GasParseError.InvalidCharacter,
            }
        } else {
            return GasParseError.UnexpectedEOF;
        }
        return;
    }

    /// Entered a Line Comment, ignores all characters until a new line is found
    ///
    ///                         ------------ You are here
    ///                         V          V
    /// /*Block Comment*/    // Line Comment
    /// [t:template,n:my_template]
    /// {
    ///     ...
    /// }
    fn parseComment() GasParseError!void {
        i.* += 1;
        while (i.* < buffer.len) : (i.* += 1) {
            switch (buffer.*[i.*]) {
                0x00 => return GasParseError.NullCharacter,
                '\n' => return,
                else => continue,
            }
        }
        return;
    }

    /// Entered a Block Comment, ignores all characters until a * is found
    ///
    ///   -------------- You are here
    ///   V            V
    /// /*Block Comment*/    // Line Comment
    /// [t:template,n:my_template]
    /// {
    ///     ...
    /// }
    fn parseCommentBlock() GasParseError!void {
        i.* += 1;
        while (i.* < buffer.len) : (i.* += 1) {
            switch (buffer.*[i.*]) {
                0x00 => return GasParseError.NullCharacter,
                '*' => if (try parseEndCommentBlock()) return else continue,
                else => continue,
            }
        }
        return GasParseError.UnexpectedEOF;
    }

    /// Parses whether the * character found in a Block Comment ends the Comment with a forward slash or if the Comment should continue
    ///
    ///                 -- You are here
    ///                 V
    /// /*Block Comment*/    // Line Comment
    /// [t:template,n:my_template]
    /// {
    ///     ...
    /// }
    fn parseEndCommentBlock() GasParseError!bool {
        i.* += 1;
        if (i.* < buffer.len) {
            switch (buffer.*[i.*]) {
                '/' => return true,
                '*' => return parseEndCommentBlock(),
                else => return false,
            }
        }
        return GasParseError.UnexpectedEOF;
    }

    /// Parses that the [ character found transitions into t: , gets the Type and Name of the Template, then transitions into a Gas Block
    ///
    ///  -- You are here
    ///  VV
    /// [t:template,n:my_template]
    /// {
    ///     ...
    /// }
    fn parseFileTxGasEntry() !void {
        i.* += 1;
        if (i.* + 1 < buffer.len) {
            switch (buffer.*[i.*]) {
                0x00 => return GasParseError.NullCharacter,
                't' => {
                    i.* += 1;
                    switch (buffer.*[i.*]) {
                        0x00 => return GasParseError.NullCharacter,
                        ':' => {
                            var gas_type: []const u8 = undefined;
                            var entry_name: []const u8 = undefined;
                            try parseGasEntryType(&gas_type);
                            try parseGasEntryTxName(&entry_name);
                            const new_entry = try gas_file.addEntry(entry_name, gas_type);
                            try parseGasTxBlock(new_entry, null);
                        },
                        else => return GasParseError.InvalidCharacter,
                    }
                },
                else => return GasParseError.InvalidCharacter,
            }
        } else {
            return GasParseError.UnexpectedEOF;
        }
        return;
    }

    /// Parses all word characters for the Template type
    ///
    ///    --------- You are here
    ///    V       V
    /// [t:template,n:my_template]
    /// {
    ///     ...
    /// }
    fn parseGasEntryType(gas_type_ptr: *[]const u8) GasParseError!void {
        i.* += 1;
        const type_start: u24 = i.*;
        while (i.* < buffer.len) : (i.* += 1) {
            switch (buffer.*[i.*]) {
                0x00 => return GasParseError.NullCharacter,
                ',' => {
                    gas_type_ptr.* = buffer.*[type_start..i.*];
                    return;
                },
                'A'...'Z', 'a'...'z' => continue,
                else => return GasParseError.InvalidCharacter,
            }
        }
        return GasParseError.UnexpectedEOF;
    }

    /// Parses that the , character found after the Template Type transitions into n: for the Template Name
    ///
    ///             -- You are here
    ///             VV
    /// [t:template,n:my_template]
    /// {
    ///     ...
    /// }
    fn parseGasEntryTxName(entry_name_ptr: *[]const u8) GasParseError!void {
        i.* += 1;
        if (i.* + 1 < buffer.len) {
            switch (buffer.*[i.*]) {
                0x00 => return GasParseError.NullCharacter,
                'n' => {
                    i.* += 1;
                    switch (buffer.*[i.*]) {
                        0x00 => return GasParseError.NullCharacter,
                        ':' => try parseGasEntryName(entry_name_ptr),
                        else => return GasParseError.InvalidCharacter,
                    }
                },
                else => return GasParseError.InvalidCharacter,
            }
        } else {
            return GasParseError.UnexpectedEOF;
        }
        return;
    }

    /// Parses all word characters and certain allowed special characters for the Template name
    ///
    ///               ------------ You are here
    ///               V          V
    /// [t:template,n:my_template]
    /// {
    ///     ...
    /// }
    fn parseGasEntryName(entry_name_ptr: *[]const u8) GasParseError!void {
        i.* += 1;
        const name_start: u24 = i.*;
        while (i.* < buffer.len) : (i.* += 1) {
            switch (buffer.*[i.*]) {
                0x00 => return GasParseError.NullCharacter,
                ']' => {
                    entry_name_ptr.* = buffer.*[name_start..i.*];
                    return;
                },
                '$', '*', '-', '.', '_' => continue,
                'A'...'Z', 'a'...'z', '0'...'9' => continue,
                else => return GasParseError.InvalidCharacter,
            }
        }
        return GasParseError.UnexpectedEOF;
    }

    /// Parses any whitespace or Comments after a Template or Component definition until the start of a Gas Block is found
    ///
    ///                           --- You are here
    ///                           V V
    /// [t:template,n:my_template]
    /// {
    ///     ...
    /// }
    fn parseGasTxBlock(gas_entry: *Gas.GasEntry, parent_component: ?*Gas.Component) (GasParseError || Allocator.Error)!void {
        i.* += 1;
        while (i.* < buffer.len) : (i.* += 1) {
            switch (buffer.*[i.*]) {
                0x00 => return GasParseError.NullCharacter,
                '\t', '\n', '\r', ' ' => continue,
                '/' => try parseStartComment(),
                '{' => {
                    try parseGasBlock(gas_entry, parent_component);
                    return;
                },
                else => return GasParseError.InvalidCharacter,
            }
        }
    }

    /// Inner layer of a Gas Block
    /// This can be either a Template Block, or a Component Block (of any nested layer)
    /// Parses out any whitespace or Comments until either an Attribute or Component is found
    /// Attribute and Component methods will return back into this method to continue the loop until a } character is found which ends the Block
    ///
    /// [t:template,n:my_template]
    /// {                              <-- You are here
    ///     attribute = value;
    ///     [my_component]
    ///     {                          <-- Or here
    ///         ...
    ///     }
    /// }
    fn parseGasBlock(gas_entry: *Gas.GasEntry, parent_component: ?*Gas.Component) !void {
        i.* += 1;
        while (i.* < buffer.len) : (i.* += 1) {
            switch (buffer.*[i.*]) {
                0x00 => return GasParseError.NullCharacter,
                '\t', '\n', '\r', ' ' => continue,
                '/' => try parseStartComment(),
                'A'...'Z', 'a'...'z', '0'...'9', '*' => {
                    var attr_name: []const u8 = undefined;
                    var attr_value: []const u8 = undefined;
                    try parseAttributeName(&attr_name);
                    if (try parseAttributeNameTxEquals()) {
                        try parseAttributeEqualsTxValue();
                        try parseAttributeValue(&attr_value);
                        if (parent_component) |parent| {
                            _ = try parent.addAttribute(attr_name, attr_value);
                        } else {
                            _ = try gas_entry.addAttribute(attr_name, attr_value);
                        }
                    } else try parseComponentAttributeSpecial(gas_entry, parent_component, &attr_name);
                },
                '[' => {
                    var cmpnt_name: []const u8 = undefined;
                    try parseComponentName(&cmpnt_name);
                    var new_parent: ?*Gas.Component = undefined;
                    if (parent_component) |parent| {
                        new_parent = try parent.addComponent(cmpnt_name);
                    } else {
                        new_parent = try gas_entry.addComponent(cmpnt_name);
                    }
                    try parseGasTxBlock(gas_entry, new_parent);
                },
                '}' => return,
                else => return GasParseError.InvalidCharacter,
            }
        }
    }

    /// Parses all word characters and certain allowed special characters until whitespace or an = character is found for the Attribute name
    /// Will also return if a : character is found, due to special cases for 'component:attribute = value' syntax; see parseAttributeNameTxEquals()
    /// This and the following Attribute methods could be for either a Template attribute or a Component Attribute
    ///
    /// [t:template,n:my_template]
    /// {
    ///     --------- You are here
    ///     V       V
    ///     attribute = value;
    ///     [my_component]
    ///     {
    ///         --------- Or here
    ///         V       V
    ///         attribute = value;
    ///     }
    /// }
    fn parseAttributeName(attr_name_ptr: *[]const u8) GasParseError!void {
        const name_start: u24 = i.*;
        i.* += 1;
        while (i.* < buffer.len) : (i.* += 1) {
            switch (buffer.*[i.*]) {
                0x00 => return GasParseError.NullCharacter,
                '\t', '\n', '\r', ' ', '=', ':' => {
                    attr_name_ptr.* = buffer.*[name_start..i.*];
                    return;
                },
                '$', '*', '-', '.', '_' => continue,
                'A'...'Z', 'a'...'z', '0'...'9' => continue,
                else => return GasParseError.InvalidCharacter,
            }
        }
        return GasParseError.UnexpectedEOF;
    }

    /// If an = character was found immediately after the Attribute name (no whitespace), immediate return
    /// Otherwise, parse whitespace until an = character is Found
    ///
    /// Because Gas allows for the syntax 'component:attribute = value', it is actually possible that this method will parse what needs to be a component
    /// Therefore, if a : character is detected, false is returned (indicating an Attribute was not what was found)
    ///
    /// [t:template,n:my_template]
    /// {
    ///              -- You are here
    ///              VV
    ///     attribute = value;
    ///     [my_component]
    ///     {
    ///                  -- Or here
    ///                  VV
    ///         attribute = value;
    ///     }
    /// }
    fn parseAttributeNameTxEquals() GasParseError!bool {
        switch (buffer.*[i.*]) {
            '=' => return true,
            ':' => return false,
            else => {
                i.* += 1;
                while (i.* < buffer.len) : (i.* += 1) {
                    switch (buffer.*[i.*]) {
                        0x00 => return GasParseError.NullCharacter,
                        '\t', '\n', '\r', ' ' => continue,
                        '=' => return true,
                        ':' => return false,
                        else => return GasParseError.InvalidCharacter,
                    }
                }
            },
        }
        return GasParseError.UnexpectedEOF;
    }

    /// Parses whitespace until any valid character for an Attribute value is found
    ///
    /// [t:template,n:my_template]
    /// {
    ///                -- You are here
    ///                V
    ///     attribute = value;
    ///     [my_component]
    ///     {
    ///                    -- Or here
    ///                    V
    ///         attribute = value;
    ///     }
    /// }
    fn parseAttributeEqualsTxValue() GasParseError!void {
        i.* += 1;
        while (i.* < buffer.len) : (i.* += 1) {
            switch (buffer.*[i.*]) {
                0x00 => return GasParseError.NullCharacter,
                '\t', '\n', '\r', ' ' => continue,
                ';' => return GasParseError.InvalidCharacter,
                else => return,
            }
        }
        return GasParseError.UnexpectedEOF;
    }

    /// Parses all characters until a ; character is found for the Attribute value
    ///
    /// [t:template,n:my_template]
    /// {
    ///                 ------ You are here
    ///                 V    V
    ///     attribute = value;
    ///     [my_component]
    ///     {
    ///                     ------ Or here
    ///                     V    V
    ///         attribute = value;
    ///     }
    /// }
    fn parseAttributeValue(attr_value_ptr: *[]const u8) GasParseError!void {
        const value_start: u24 = i.*;
        i.* += 1;
        while (i.* < buffer.len) : (i.* += 1) {
            switch (buffer.*[i.*]) {
                0x00 => return GasParseError.NullCharacter,
                '\t', '\n', '\r', ' ' => continue,
                'A'...'Z', 'a'...'z', '0'...'9' => continue,
                '!'...'.' => continue, // ASCII 33-46 [!"#$%&'()*+,-.]
                '/' => continue, // Currently Comments in Attribute Values are included as part of the Value
                ':' => continue,
                ';' => {
                    attr_value_ptr.* = buffer.*[value_start..i.*];
                    return;
                },
                '<'...'@' => continue, // ASCII 60-64 [<=>?@]
                '['...'`' => continue, // ASCII 91-96 [\[\\\]^_`]
                '{'...'~' => continue, // ASCII 123-126 [{|}~]
                else => return GasParseError.InvalidCharacter,
            }
        }
        return GasParseError.UnexpectedEOF;
    }

    /// Parses all word characters and certain allowed special characters until a ] character is found for the Component name
    ///
    /// [t:template,n:my_template]
    /// {
    ///      ------------- You are here
    ///      V           V
    ///     [my_component]
    ///     {
    ///       ...
    ///     }
    /// }
    fn parseComponentName(cmpnt_name_ptr: *[]const u8) GasParseError!void {
        i.* += 1;
        const name_start: u24 = i.*;
        while (i.* < buffer.len) : (i.* += 1) {
            switch (buffer.*[i.*]) {
                0x00 => return GasParseError.NullCharacter,
                ']' => {
                    cmpnt_name_ptr.* = buffer.*[name_start..i.*];
                    return;
                },
                '$', '*', '-', '.', '_' => continue,
                'A'...'Z', 'a'...'z', '0'...'9' => continue,
                else => return GasParseError.InvalidCharacter,
            }
        }
        return GasParseError.UnexpectedEOF;
    }

    /// Special handling for syntax 'component:attribute = value'
    ///
    /// [t:template,n:my_template]
    /// {
    ///                 -- You are here
    ///                 V
    ///     my_component:attribute = value;
    ///     [my_component2]
    ///     {
    ///                      -- Or here
    ///                      V
    ///         my_component3:attribute = value;
    ///     }
    /// }
    fn parseComponentAttributeSpecial(gas_entry: *Gas.GasEntry, parent_component: ?*Gas.Component, cmpnt_name_ptr: *[]const u8) !void {
        var new_parent: ?*Gas.Component = undefined;
        if (parent_component) |parent| {
            new_parent = try parent.addComponent(cmpnt_name_ptr.*);
        } else {
            new_parent = try gas_entry.addComponent(cmpnt_name_ptr.*);
        }

        i.* += 1;
        while (i.* < buffer.len) : (i.* += 1) {
            switch (buffer.*[i.*]) {
                0x00 => return GasParseError.NullCharacter,
                '\t', '\n', '\r', ' ' => continue,
                'A'...'Z', 'a'...'z', '0'...'9', '$', '*', '-', '.', '_' => {
                    var attr_name: []const u8 = undefined;
                    var attr_value: []const u8 = undefined;
                    try parseAttributeName(&attr_name);
                    if (try parseAttributeNameTxEquals()) {
                        try parseAttributeEqualsTxValue();
                        try parseAttributeValue(&attr_value);
                        _ = try new_parent.?.addAttribute(attr_name, attr_value);
                        return;
                    } else try parseComponentAttributeSpecial(gas_entry, new_parent, &attr_name);
                },
                else => return GasParseError.InvalidCharacter,
            }
        }
    }
};

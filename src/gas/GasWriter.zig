const std = @import("std");
const ArrayList = std.ArrayList;
const SegmentedList = std.SegmentedList;
const Allocator = std.mem.Allocator;
const File = std.fs.File;
const Timer = std.time.Timer;

const Gas = @import("Gas.zig");

const VERSION: u8 = 1;

/// Writes a given list of GasFiles to the output directory ("/output_templates/{file_path}")
/// Frees files from memory after writing
pub fn writeAll(allocator: Allocator, gas_files: *SegmentedList(Gas.GasFile, 32), output_path: []const u8) !void {
    var total_timer = try Timer.start();

    var it = gas_files.iterator(0);
    while (it.next()) |file| {
        try write(allocator, file, output_path);
        file.deinit();
    }

    std.log.info("Total Time Taken To Write: {d:.4}ms\n\n------------------------\n", .{
        @as(f128, @floatFromInt(total_timer.lap())) / @as(f32, @floatFromInt(std.time.ns_per_ms)),
    });
}

/// Writes a single GasFile to the given output directory
pub fn write(allocator: Allocator, file: *Gas.GasFile, output_path: []const u8) !void {
    var cwd = std.fs.cwd();
    const output_dir_str = try std.fmt.allocPrint(allocator, "{s}/{s}", .{ output_path, file.file_path });
    defer allocator.free(output_dir_str);
    const output_dir = cwd.openDir(output_dir_str, .{ .iterate = true }) catch blk: {
        try cwd.makePath(output_dir_str);
        break :blk try cwd.openDir(output_dir_str, .{ .iterate = true });
    };

    var output_file = try output_dir.createFile(file.file_name, .{});
    const gas_file_bytes = try gasFileToBytes(allocator, file);
    _ = try output_file.write(gas_file_bytes);
    allocator.free(gas_file_bytes);
}

/// Translates a given GasFile into a byte array
fn gasFileToBytes(allocator: Allocator, file: *Gas.GasFile) ![]const u8 {
    var buffer: ArrayList(u8) = try ArrayList(u8).initCapacity(allocator, predictFileSize(file));

    var e = file.entries.first;
    while (e) |e_node| : (e = e_node.next) {
        try buffer.appendSlice("[t:");
        try buffer.appendSlice(e_node.data.gas_type);
        try buffer.appendSlice(",n:");
        try buffer.appendSlice(e_node.data.name);
        try buffer.appendSlice("]\n{\n");
        try gasEntryToBytes(&buffer, &e_node.data);
        try buffer.appendSlice("}\n");
    }

    return try buffer.toOwnedSlice();
}

/// Translates a given GasEntry into a byte array
fn gasEntryToBytes(buffer: *ArrayList(u8), entry: *Gas.GasEntry) !void {
    var indent: usize = 1;
    var i: usize = 0;

    var a = entry.attributes.first;
    while (a) |a_node| : (a = a_node.next) {
        while (i < indent) : (i += 1) {
            try buffer.appendSlice("    ");
        }
        i = 0;
        try buffer.appendSlice(a_node.data.name);
        try buffer.appendSlice(" = ");
        try buffer.appendSlice(a_node.data.value);
        try buffer.appendSlice(";\n");
    }

    var c = entry.components.first;
    while (c) |c_node| : (c = c_node.next) {
        indent = 1;
        while (i < indent) : (i += 1) {
            try buffer.appendSlice("    ");
        }
        i = 0;
        try buffer.append('[');
        try buffer.appendSlice(c_node.data.name);
        try buffer.appendSlice("]\n");
        while (i < indent) : (i += 1) {
            try buffer.appendSlice("    ");
        }
        i = 0;
        try buffer.appendSlice("{\n");
        try gasComponentToBytes(buffer, &c_node.data, &indent);
        indent -= 1;
        while (i < indent) : (i += 1) {
            try buffer.appendSlice("    ");
        }
        i = 0;
        try buffer.appendSlice("}\n");
    }
}

/// Translates a given Component into a byte array
fn gasComponentToBytes(buffer: *ArrayList(u8), component: *Gas.Component, indent: *usize) !void {
    indent.* += 1;
    var i: usize = 0;

    var a = component.attributes.first;
    while (a) |a_node| : (a = a_node.next) {
        while (i < indent.*) : (i += 1) {
            try buffer.appendSlice("    ");
        }
        i = 0;
        try buffer.appendSlice(a_node.data.name);
        try buffer.appendSlice(" = ");
        try buffer.appendSlice(a_node.data.value);
        try buffer.appendSlice(";\n");
    }

    var c = component.components.first;
    while (c) |c_node| : (c = c_node.next) {
        while (i < indent.*) : (i += 1) {
            try buffer.appendSlice("    ");
        }
        i = 0;
        try buffer.append('[');
        try buffer.appendSlice(c_node.data.name);
        try buffer.appendSlice("]\n");
        while (i < indent.*) : (i += 1) {
            try buffer.appendSlice("    ");
        }
        i = 0;
        try buffer.appendSlice("{\n");
        try gasComponentToBytes(buffer, &c_node.data, indent);
        indent.* -= 1;
        while (i < indent.*) : (i += 1) {
            try buffer.appendSlice("    ");
        }
        i = 0;
        try buffer.appendSlice("}\n");
    }
}

/// Returns an estimate of the number of bytes that it will take to write a given GasFile
/// Esimate is made by adding up the lengths of all entry names/types, component names, attribute names/values, spacing and special characters for gas syntax
/// Initializing the buffer with estimated capacity in this manner minmizes reallocs for the ArrayList and reduces write time by ~20%
fn predictFileSize(file: *Gas.GasFile) usize {
    var size: usize = 0;

    var e = file.entries.first;
    while (e) |e_node| : (e = e_node.next) {
        size += 12 + e_node.data.name.len + e_node.data.gas_type.len + predictEntrySize(&e_node.data);
    }

    return size;
}

/// Returns an estimate of the number of bytes that it will take to write a given GasEntry
fn predictEntrySize(entry: *Gas.GasEntry) usize {
    var size: usize = 0;
    var indent: usize = 4;

    var a = entry.attributes.first;
    while (a) |a_node| : (a = a_node.next) {
        size += 5 + indent + a_node.data.name.len + a_node.data.value.len;
    }

    var c = entry.components.first;
    while (c) |c_node| : (c = c_node.next) {
        indent = 4;
        size += 7 + indent + c_node.data.name.len + predictComponentSize(&c_node.data, &indent);
    }

    return size;
}

/// Returns an estimate of the number of bytes that it will take to write a given Component
fn predictComponentSize(component: *Gas.Component, indent: *usize) usize {
    var size: usize = 0;
    indent.* += 4;

    var a = component.attributes.first;
    while (a) |a_node| : (a = a_node.next) {
        size += 5 + indent.* + a_node.data.name.len + a_node.data.value.len;
    }

    var c = component.components.first;
    while (c) |c_node| : (c = c_node.next) {
        size += 7 + indent.* + c_node.data.name.len + predictComponentSize(&c_node.data, indent);
    }

    return size;
}

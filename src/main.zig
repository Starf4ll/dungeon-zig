const std = @import("std");
const GenerateMonsters = @import("scripts/generate_monsters/GenerateMonsters.zig");
const GenerateContainers = @import("scripts/generate_containers/GenerateContainers.zig");

pub fn main() !void {
    const stdin = std.io.getStdIn().reader();
    const stdout = std.io.getStdOut().writer();

    var buf: [10]u8 = undefined;
    while (true) {
        try stdout.print("Choose one of the following:\n  0 : Exit\n  1 : Generate Monsters\n  2 : Generate Containers\n\n> ", .{});
        if (try stdin.readUntilDelimiterOrEof(buf[0..], '\n')) |user_input| {
            const trimmed_input = std.mem.trimRight(u8, user_input[0 .. user_input.len - 1], "\r");
            const option = std.fmt.parseInt(i64, trimmed_input, 10) catch {
                try stdout.print("\nThat is not a valid option\n\n", .{});
                continue;
            };
            switch (option) {
                0 => break,
                1 => {
                    GenerateMonsters.generateMonsters() catch |err| {
                        if (@errorReturnTrace()) |trace| {
                            std.debug.dumpStackTrace(trace.*);
                        }
                        try stdout.print("\nAn error occurred during generateMonsters(): {}\n\nPress Enter to continue\n", .{err});
                        _ = try stdin.readUntilDelimiterOrEof(buf[0..], '\n');
                    };
                },
                2 => {
                    GenerateContainers.generateContainers() catch |err| {
                        if (@errorReturnTrace()) |trace| {
                            std.debug.dumpStackTrace(trace.*);
                        }
                        try stdout.print("\nAn error occurred during generateContainers(): {}\n\nPress Enter to continue\n", .{err});
                        _ = try stdin.readUntilDelimiterOrEof(buf[0..], '\n');
                    };
                },
                else => try stdout.print("\nThat is not a valid option\n\n", .{}),
            }
        } else {
            std.debug.print("\n", .{});
        }
    }
}

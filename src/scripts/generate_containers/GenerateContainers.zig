const std = @import("std");
const ArrayList = std.ArrayList;
const SegmentedList = std.SegmentedList;
const Allocator = std.mem.Allocator;
const ArenaAllocator = std.heap.ArenaAllocator;
const Timer = std.time.Timer;
const allocPrint = std.fmt.allocPrint;
const pow = std.math.pow;

const Gas = @import("../../gas/Gas.zig");
const GasParser = @import("../../gas/GasParser.zig");
const GasWriter = @import("../../gas/GasWriter.zig");
const Csv = @import("../../csv/Csv.zig");
const Formulas = @import("GenerateContainersFormulas.zig");
const ContainersMap = @import("ContainersMap.zig");

const WORLD_DIFFS = [3][]const u8{ "regular", "veteran", "elite" };
const WORLD_DIFF_PREFIXES = [3][]const u8{ "", "2W_", "3W_" };
const CATEGORY_WORLD_DIFF_PREFIXES = [3][]const u8{ "1W_", "2W_", "3W_" };

/// Generates containers for Dungeon Siege
/// Reads rows from a CSV to determine template attributes, then creates and writes templates to the output directory
pub fn generateContainers() !void {
    var total_timer = try Timer.start();

    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();

    const containers_csv = try Csv.CsvParser.parse(allocator, "src/scripts/generate_containers/generate_containers.csv", ',');
    var new_gas_files = SegmentedList(Gas.GasFile, 32){};

    defer containers_csv.deinit();
    defer new_gas_files.deinit(allocator);

    for (0..3) |diff_index| {
        const out_file = try new_gas_files.addOne(allocator);
        const file_path = try allocPrint(allocator, "{s}\\interactive\\containers", .{WORLD_DIFFS[diff_index]});
        const file_name = try allocPrint(allocator, "{s}doh_containers_regional.gas", .{WORLD_DIFF_PREFIXES[diff_index]});
        _ = try Gas.GasFile.init(allocator, out_file, file_path, file_name);
        allocator.free(file_path);
        allocator.free(file_name);
        for (containers_csv.rows) |row| {
            try generateContainersInFile(allocator, &row, out_file, diff_index);
        }
    }

    std.log.info("Total Time Taken To Generate: {d:.4}ms", .{
        @as(f128, @floatFromInt(total_timer.lap())) / @as(f32, @floatFromInt(std.time.ns_per_ms)),
    });

    try GasWriter.writeAll(allocator, &new_gas_files, "src/scripts/generate_containers/output");
}

/// Generates new templates for containers
fn generateContainersInFile(
    allocator: Allocator,
    row: *const [][]const u8,
    out_file: *Gas.GasFile,
    diff_index: usize,
) !void {
    const prefixed_name = try allocPrint(allocator, "{s}{s}", .{ WORLD_DIFF_PREFIXES[diff_index], row.*[0][1 .. row.*[0].len - 1] });
    const out_tmpl = try out_file.addEntry(prefixed_name, "template");
    allocator.free(prefixed_name);

    const container_level: f32 = try std.fmt.parseFloat(f32, row.*[2 + diff_index]);
    const container_attrs = ContainersMap.ContainersMap.get(row.*[5][1 .. row.*[5].len - 1]);

    if (container_attrs) |*attrs| {
        try addBaseAttributes(allocator, &row.*[0][1..], &row.*[1], &attrs.specializes, out_tmpl, diff_index);
        try addAspectComponent(attrs, out_tmpl);
        try addAttackComponent(allocator, &container_level, &attrs.damage_scalar, out_tmpl);
        try addBodyComponent(attrs, out_tmpl);
        try addCommonComponent(allocator, attrs, out_tmpl);
        try addInventoryComponent(allocator, &container_level, attrs, out_tmpl);
        try addTrapComponent(out_tmpl, &row.*[6], &attrs.fire_event);
    } else {
        std.log.warn("[{s}] is using an unknown container_type: [{s}]! Output Template will be empty.", .{ row.*[0], row.*[5] });
    }
}

/// Adds the base level Attributes for the given template
fn addBaseAttributes(
    allocator: Allocator,
    template_name: *const []const u8,
    category: *const []const u8,
    specializes: *const []const u8,
    out_tmpl: *Gas.GasEntry,
    diff_index: usize,
) !void {
    _ = try out_tmpl.addAttribute("category_name", category.*);

    const doc_value = try allocPrint(allocator, "\"{s}{s}", .{ CATEGORY_WORLD_DIFF_PREFIXES[diff_index], template_name.* });
    _ = try out_tmpl.addAttribute("doc", doc_value);
    allocator.free(doc_value);

    _ = try out_tmpl.addAttribute("specializes", specializes.*);
}

/// Adds the Aspect Component for the given template
fn addAspectComponent(
    container_attrs: *const ContainersMap.ContainerAttributes,
    out_tmpl: *Gas.GasEntry,
) !void {
    var out_aspect = try out_tmpl.addComponent("aspect");

    if (container_attrs.material) |material| {
        _ = try out_aspect.addAttribute("material", material);
    }
    _ = try out_aspect.addAttribute("model", container_attrs.model);
    if (container_attrs.texture) |texture| {
        var out_textures = try out_aspect.addComponent("textures");
        _ = try out_textures.addAttribute("0", texture);
    }
}

/// Adds the Attack Component for the given template
/// Damage is based on the container level
fn addAttackComponent(
    allocator: Allocator,
    container_level: *const f32,
    damage_scalar: *const f32,
    out_tmpl: *Gas.GasEntry,
) !void {
    var out_attack = try out_tmpl.addComponent("attack");

    const min_dmg = try allocPrint(allocator, "{d}", .{@round(Formulas.minDamage(container_level.*, damage_scalar.*))});
    const max_dmg = try allocPrint(allocator, "{d}", .{@round(Formulas.maxDamage(container_level.*, damage_scalar.*))});
    _ = try out_attack.addAttribute("damage_min", min_dmg);
    _ = try out_attack.addAttribute("damage_max", max_dmg);
    allocator.free(min_dmg);
    allocator.free(max_dmg);
}

/// Adds the Body Component for the given template
fn addBodyComponent(
    container_attrs: *const ContainersMap.ContainerAttributes,
    out_tmpl: *Gas.GasEntry,
) !void {
    if (container_attrs.chore_prefix) |chore_prefix| {
        var out_body = try out_tmpl.addComponent("body");

        var out_chore_dictionary = try out_body.addComponent("chore_dictionary");
        _ = try out_chore_dictionary.addAttribute("chore_prefix", chore_prefix);

        if (container_attrs.chore_open_00) |chore_open_00| {
            var out_chore_open = try out_chore_dictionary.addComponent("chore_open");
            _ = try out_chore_open.addAttribute("skrit", container_attrs.chore_open_skrit);
            var out_chore_open_anim = try out_chore_open.addComponent("anim_files");
            _ = try out_chore_open_anim.addAttribute("00", chore_open_00);
            if (container_attrs.chore_open_01) |chore_open_01| {
                _ = try out_chore_open_anim.addAttribute("01", chore_open_01);
            }
        }

        if (container_attrs.chore_close_00) |chore_close_00| {
            var out_chore_close = try out_chore_dictionary.addComponent("chore_close");
            _ = try out_chore_close.addAttribute("skrit", container_attrs.chore_close_skrit);
            var out_chore_close_anim = try out_chore_close.addComponent("anim_files");
            _ = try out_chore_close_anim.addAttribute("00", chore_close_00);
            if (container_attrs.chore_close_01) |chore_close_01| {
                _ = try out_chore_close_anim.addAttribute("01", chore_close_01);
            }
        }
    }
}

/// Adds the Common Component for the given template
fn addCommonComponent(
    allocator: Allocator,
    container_attrs: *const ContainersMap.ContainerAttributes,
    out_tmpl: *Gas.GasEntry,
) !void {
    if (container_attrs.sfx_script) |sfx_script| {
        const sfx_script_str = try allocPrint(allocator, "call_sfx_script(\"{s}\")", .{sfx_script});
        defer allocator.free(sfx_script_str);

        var out_common = try out_tmpl.addComponent("common");

        var out_template_triggers = try out_common.addComponent("template_triggers");

        var out_template_triggers_star = try out_template_triggers.addComponent("*");
        _ = try out_template_triggers_star.addAttribute("condition*", "receive_world_message(\"WE_ENTERED_WORLD\")");
        _ = try out_template_triggers_star.addAttribute("action*", sfx_script_str);
    }
}

/// Adds the Inventory Component for the given template
/// Item drops are based on attributes from ContainersMap, multipliers from generate_containers.csv and the formulas in GenerateContainersFormulas
fn addInventoryComponent(
    allocator: Allocator,
    container_level: *const f32,
    container_attrs: *const ContainersMap.ContainerAttributes,
    out_tmpl: *Gas.GasEntry,
) !void {
    var out_inventory = try out_tmpl.addComponent("inventory");
    var out_pcontent = try out_inventory.addComponent("pcontent");

    const gold_min_str = try allocPrint(allocator, "{d}", .{@round(Formulas.minGoldDrop(container_level.*, container_attrs))});
    const gold_max_str = try allocPrint(allocator, "{d}", .{@round(Formulas.maxGoldDrop(container_level.*, container_attrs))});

    const spell_str = try allocPrint(allocator, "#spell/{d}-{d}", .{
        @round(Formulas.minSpellDropPower(container_level.*, container_attrs)),
        @round(Formulas.maxSpellDropPower(container_level.*, container_attrs)),
    });

    const min_power = Formulas.minItemDropPower(container_level.*);
    const max_power = Formulas.maxItemDropPower(container_level.*);

    const common_min_power = min_power * container_attrs.common_mult;
    const common_max_power = max_power * container_attrs.common_mult;
    const common_armor_str = try allocPrint(allocator, "#armor/{d}-{d}", .{ @round(common_min_power * 2.2), @round(common_max_power * 2.2) });
    const common_weapon_str = try allocPrint(allocator, "#weapon/{d}-{d}", .{ @round(common_min_power), @round(common_max_power) });
    const common_item_str = try allocPrint(allocator, "#*/{d}-{d}", .{ @round(common_min_power), @round(common_max_power) });

    const rare_1_min_power = min_power * 1.04 * container_attrs.rare1_mult;
    const rare_1_max_power = max_power * 1.04 * container_attrs.rare1_mult;
    const rare_1_armor_str = try allocPrint(allocator, "#armor/-rare(1)/{d}-{d}", .{ @round(rare_1_min_power * 2.2), @round(rare_1_max_power * 2.2) });
    const rare_1_weapon_str = try allocPrint(allocator, "#weapon/-rare(1)/{d}-{d}", .{ @round(rare_1_min_power), @round(rare_1_max_power) });
    const rare_1_item_str = try allocPrint(allocator, "#*/-rare(1)/{d}-{d}", .{ @round(rare_1_min_power), @round(rare_1_max_power) });

    const rare_2_min_power = min_power * 1.04 * container_attrs.rare2_mult;
    const rare_2_max_power = max_power * 1.04 * container_attrs.rare2_mult;
    const rare_2_armor_str = try allocPrint(allocator, "#armor/-rare(2)/{d}-{d}", .{ @round(rare_2_min_power * 2.2), @round(rare_2_max_power * 2.2) });
    const rare_2_weapon_str = try allocPrint(allocator, "#weapon/-rare(2)/{d}-{d}", .{ @round(rare_2_min_power), @round(rare_2_max_power) });
    const rare_2_item_str = try allocPrint(allocator, "#*/-rare(2)/{d}-{d}", .{ @round(rare_2_min_power), @round(rare_2_max_power) });

    const unique_1_min_power = min_power * 1.08 * container_attrs.unique1_mult;
    const unique_1_max_power = max_power * 1.08 * container_attrs.unique1_mult;
    const unique_1_armor_str = try allocPrint(allocator, "#armor/-unique(1)/{d}-{d}", .{ @round(unique_1_min_power * 2.2), @round(unique_1_max_power * 2.2) });
    const unique_1_weapon_str = try allocPrint(allocator, "#weapon/-unique(1)/{d}-{d}", .{ @round(unique_1_min_power), @round(unique_1_max_power) });
    const unique_1_item_str = try allocPrint(allocator, "#*/-unique(1)/{d}-{d}", .{ @round(unique_1_min_power), @round(unique_1_max_power) });

    const unique_2_min_power = min_power * 1.08 * container_attrs.unique2_mult;
    const unique_2_max_power = max_power * 1.08 * container_attrs.unique2_mult;
    const unique_2_armor_str = try allocPrint(allocator, "#armor/-unique(2)/{d}-{d}", .{ @round(unique_2_min_power * 2.2), @round(unique_2_max_power * 2.2) });
    const unique_2_weapon_str = try allocPrint(allocator, "#weapon/-unique(2)/{d}-{d}", .{ @round(unique_2_min_power), @round(unique_2_max_power) });
    const unique_2_item_str = try allocPrint(allocator, "#*/-unique(2)/{d}-{d}", .{ @round(unique_2_min_power), @round(unique_2_max_power) });

    defer allocator.free(gold_min_str);
    defer allocator.free(gold_max_str);
    defer allocator.free(spell_str);
    defer allocator.free(common_armor_str);
    defer allocator.free(common_weapon_str);
    defer allocator.free(common_item_str);
    defer allocator.free(rare_1_armor_str);
    defer allocator.free(rare_1_weapon_str);
    defer allocator.free(rare_1_item_str);
    defer allocator.free(rare_2_armor_str);
    defer allocator.free(rare_2_weapon_str);
    defer allocator.free(rare_2_item_str);
    defer allocator.free(unique_1_armor_str);
    defer allocator.free(unique_1_weapon_str);
    defer allocator.free(unique_1_item_str);
    defer allocator.free(unique_2_armor_str);
    defer allocator.free(unique_2_weapon_str);
    defer allocator.free(unique_2_item_str);

    var all = try out_pcontent.addComponent("all*");

    if (container_attrs.gold_chance) |gold_chance| {
        var gold = try all.addComponent("gold*");
        _ = try gold.addAttribute("chance", gold_chance);
        _ = try gold.addAttribute("min", gold_min_str);
        _ = try gold.addAttribute("max", gold_max_str);
        if (container_attrs.gold_piles) |gold_piles| {
            _ = try gold.addAttribute("piles", gold_piles);
        }
    }

    if (container_attrs.potion_chance) |potion_chance| {
        var health_potion_template: []const u8 = undefined;
        var mana_potion_template: []const u8 = undefined;
        if (container_level.* < 12) {
            health_potion_template = "potion_health_small";
            mana_potion_template = "potion_mana_small";
        } else if (container_level.* < 35) {
            health_potion_template = "potion_health_medium";
            mana_potion_template = "potion_mana_medium";
        } else if (container_level.* < 70) {
            health_potion_template = "potion_health_large";
            mana_potion_template = "potion_mana_large";
        } else {
            health_potion_template = "potion_health_super";
            mana_potion_template = "potion_mana_super";
        }

        var potion = try all.addComponent("oneof*");
        _ = try potion.addAttribute("chance", potion_chance);
        _ = try potion.addAttribute("il_main", health_potion_template);
        _ = try potion.addAttribute("il_main", mana_potion_template);
        if (container_attrs.potion_min_qty) |potion_min_qty| {
            _ = try potion.addAttribute("min", potion_min_qty);
        }
        if (container_attrs.potion_max_qty) |potion_max_qty| {
            _ = try potion.addAttribute("max", potion_max_qty);
        }
    }

    if (container_attrs.spell_chance) |spell_chance| {
        var spells = try all.addComponent("oneof*");
        _ = try spells.addAttribute("chance", spell_chance);
        _ = try spells.addAttribute("il_main", spell_str);
        if (container_attrs.spell_min_qty) |spell_min_qty| {
            _ = try spells.addAttribute("min", spell_min_qty);
        }
        if (container_attrs.spell_max_qty) |spell_max_qty| {
            _ = try spells.addAttribute("max", spell_max_qty);
        }
    }

    if (container_attrs.common_chance) |common_chance| {
        var common_items = try all.addComponent("oneof*");
        _ = try common_items.addAttribute("chance", common_chance);
        _ = try common_items.addAttribute("il_main", common_armor_str);
        _ = try common_items.addAttribute("il_main", common_weapon_str);
        _ = try common_items.addAttribute("il_main", common_item_str);
        if (container_attrs.common_min_qty) |common_min_qty| {
            _ = try common_items.addAttribute("min", common_min_qty);
        }
        if (container_attrs.common_max_qty) |common_max_qty| {
            _ = try common_items.addAttribute("max", common_max_qty);
        }
    }

    if (container_attrs.rare1_chance) |rare1_chance| {
        var rare_1_items = try all.addComponent("oneof*");
        _ = try rare_1_items.addAttribute("chance", rare1_chance);
        _ = try rare_1_items.addAttribute("il_main", rare_1_armor_str);
        _ = try rare_1_items.addAttribute("il_main", rare_1_weapon_str);
        _ = try rare_1_items.addAttribute("il_main", rare_1_item_str);
        if (container_attrs.rare1_min_qty) |rare1_min_qty| {
            _ = try rare_1_items.addAttribute("min", rare1_min_qty);
        }
        if (container_attrs.rare1_max_qty) |rare1_max_qty| {
            _ = try rare_1_items.addAttribute("max", rare1_max_qty);
        }
    }

    if (container_attrs.rare2_chance) |rare2_chance| {
        var rare_2_items = try all.addComponent("oneof*");
        _ = try rare_2_items.addAttribute("chance", rare2_chance);
        _ = try rare_2_items.addAttribute("il_main", rare_2_armor_str);
        _ = try rare_2_items.addAttribute("il_main", rare_2_weapon_str);
        _ = try rare_2_items.addAttribute("il_main", rare_2_item_str);
        if (container_attrs.rare2_min_qty) |rare2_min_qty| {
            _ = try rare_2_items.addAttribute("min", rare2_min_qty);
        }
        if (container_attrs.rare2_max_qty) |rare2_max_qty| {
            _ = try rare_2_items.addAttribute("max", rare2_max_qty);
        }
    }

    if (container_attrs.unique1_chance) |unique1_chance| {
        var unique_1_items = try all.addComponent("oneof*");
        _ = try unique_1_items.addAttribute("chance", unique1_chance);
        _ = try unique_1_items.addAttribute("il_main", unique_1_armor_str);
        _ = try unique_1_items.addAttribute("il_main", unique_1_weapon_str);
        _ = try unique_1_items.addAttribute("il_main", unique_1_item_str);
        if (container_attrs.unique1_min_qty) |unique1_min_qty| {
            _ = try unique_1_items.addAttribute("min", unique1_min_qty);
        }
        if (container_attrs.unique1_max_qty) |unique1_max_qty| {
            _ = try unique_1_items.addAttribute("max", unique1_max_qty);
        }
    }

    if (container_attrs.unique2_chance) |unique2_chance| {
        var unique_2_items = try all.addComponent("oneof*");
        _ = try unique_2_items.addAttribute("chance", unique2_chance);
        _ = try unique_2_items.addAttribute("il_main", unique_2_armor_str);
        _ = try unique_2_items.addAttribute("il_main", unique_2_weapon_str);
        _ = try unique_2_items.addAttribute("il_main", unique_2_item_str);
        if (container_attrs.unique2_min_qty) |unique2_min_qty| {
            _ = try unique_2_items.addAttribute("min", unique2_min_qty);
        }
        if (container_attrs.unique2_max_qty) |unique2_max_qty| {
            _ = try unique_2_items.addAttribute("max", unique2_max_qty);
        }
    }

    if (Formulas.IS_DOH and container_attrs.contains_scrolls) {
        try addDohScrolls(allocator, out_pcontent, container_level, container_attrs);
    }
}

/// Adds custom item drops for Depth Of Horizon containers
fn addDohScrolls(
    allocator: Allocator,
    out_pcontent: *Gas.Component,
    container_level: *const f32,
    container_attrs: *const ContainersMap.ContainerAttributes,
) !void {
    var scroll_power = @ceil(1 + (0.0029 * pow(f32, container_level.*, 1.9)) + (1.65 * pow(f32, container_level.*, 0.75)));
    if (scroll_power > 190) scroll_power = 190;
    const scroll_min_power = @ceil(scroll_power / 1.6);

    const magic_scroll_chance = try allocPrint(allocator, "{d:.4}", .{0.0065 * container_attrs.magic_scroll_mult});
    const imbued_neg_scroll_chance = try allocPrint(allocator, "{d:.4}", .{0.0035 * container_attrs.imbued_neg_scroll_mult});
    const uncommon_scroll_chance = try allocPrint(allocator, "{d:.4}", .{(0.0028 + (0.000007 * container_level.*)) * container_attrs.uncommon_scroll_mult});
    const rare_scroll_chance = try allocPrint(allocator, "{d:.4}", .{(0.0016 + (0.000007 * container_level.*)) * container_attrs.rare_scroll_mult});
    const unique_scroll_chance = try allocPrint(allocator, "{d:.4}", .{(0.001 + (0.000007 * container_level.*)) * container_attrs.unique_scroll_mult});
    const runic_leg_scroll_chance = try allocPrint(allocator, "{d:.4}", .{0.0005 * container_attrs.runic_leg_scroll_mult});
    const artifact_scroll_chance = try allocPrint(allocator, "{d:.4}", .{0.0002 * container_attrs.artifact_scroll_mult});

    const magic_scroll_value = try allocPrint(allocator, "#scroll/{d}-{d}", .{ 1000 + scroll_min_power, 1000 + scroll_power });
    const imbued_scroll_value = try allocPrint(allocator, "#scroll/{d}-{d}", .{ 1200 + scroll_min_power, 1200 + scroll_power });
    const negative_scroll_value = try allocPrint(allocator, "#scroll/{d}-{d}", .{ 1600 + scroll_min_power, 1600 + scroll_power });
    const uncommon_scroll_value = try allocPrint(allocator, "#scroll/{d}-{d}", .{ 1400 + scroll_min_power, 1400 + scroll_power });
    const rare_scroll_value = try allocPrint(allocator, "#scroll/{d}-{d}", .{ 1800 + scroll_min_power, 1800 + scroll_power });
    const unique_scroll_value = try allocPrint(allocator, "#scroll/{d}-{d}", .{ 2200 + scroll_min_power, 2200 + scroll_power });
    const runic_scroll_value = try allocPrint(allocator, "#scroll/{d}-{d}", .{ 2000 + scroll_min_power, 2000 + scroll_power });
    const legendary_scroll_value = try allocPrint(allocator, "#scroll/{d}-{d}", .{ 2400 + scroll_min_power, 2400 + scroll_power });
    const artifact_scroll_value = try allocPrint(allocator, "#scroll/{d}-{d}", .{ 2600 + scroll_min_power, 2600 + scroll_power });

    defer allocator.free(magic_scroll_chance);
    defer allocator.free(imbued_neg_scroll_chance);
    defer allocator.free(uncommon_scroll_chance);
    defer allocator.free(rare_scroll_chance);
    defer allocator.free(unique_scroll_chance);
    defer allocator.free(runic_leg_scroll_chance);
    defer allocator.free(artifact_scroll_chance);
    defer allocator.free(magic_scroll_value);
    defer allocator.free(imbued_scroll_value);
    defer allocator.free(negative_scroll_value);
    defer allocator.free(uncommon_scroll_value);
    defer allocator.free(rare_scroll_value);
    defer allocator.free(unique_scroll_value);
    defer allocator.free(runic_scroll_value);
    defer allocator.free(legendary_scroll_value);
    defer allocator.free(artifact_scroll_value);

    // Add enchantment scrolls
    var all_scroll = try out_pcontent.addComponent("all*");
    var one_of_magic_scroll = try all_scroll.addComponent("oneof*");
    _ = try one_of_magic_scroll.addAttribute("chance", magic_scroll_chance);
    _ = try one_of_magic_scroll.addAttribute("il_main", magic_scroll_value);
    var one_of_imbued_or_negative_scroll = try all_scroll.addComponent("oneof*");
    _ = try one_of_imbued_or_negative_scroll.addAttribute("chance", imbued_neg_scroll_chance);
    _ = try one_of_imbued_or_negative_scroll.addAttribute("il_main", imbued_scroll_value);
    _ = try one_of_imbued_or_negative_scroll.addAttribute("il_main", negative_scroll_value);
    var one_of_uncommon_scroll = try all_scroll.addComponent("oneof*");
    _ = try one_of_uncommon_scroll.addAttribute("chance", uncommon_scroll_chance);
    _ = try one_of_uncommon_scroll.addAttribute("il_main", uncommon_scroll_value);
    var one_of_rare_scroll = try all_scroll.addComponent("oneof*");
    _ = try one_of_rare_scroll.addAttribute("chance", rare_scroll_chance);
    _ = try one_of_rare_scroll.addAttribute("il_main", rare_scroll_value);
    var one_of_unique_scroll = try all_scroll.addComponent("oneof*");
    _ = try one_of_unique_scroll.addAttribute("chance", unique_scroll_chance);
    _ = try one_of_unique_scroll.addAttribute("il_main", unique_scroll_value);
    if (scroll_power > 45) {
        var one_of_runic_or_legendary_scroll = try all_scroll.addComponent("oneof*");
        _ = try one_of_runic_or_legendary_scroll.addAttribute("chance", runic_leg_scroll_chance);
        _ = try one_of_runic_or_legendary_scroll.addAttribute("il_main", runic_scroll_value);
        _ = try one_of_runic_or_legendary_scroll.addAttribute("il_main", legendary_scroll_value);
        if (scroll_power > 80) {
            var one_of_artifact_scroll = try all_scroll.addComponent("oneof*");
            _ = try one_of_artifact_scroll.addAttribute("chance", artifact_scroll_chance);
            _ = try one_of_artifact_scroll.addAttribute("il_main", artifact_scroll_value);
        }
    }
}

/// Adds the Trapped Component for the given template
fn addTrapComponent(
    out_tmpl: *Gas.GasEntry,
    trap: *const []const u8,
    fire_event: *const ?[]const u8,
) !void {
    var out_trapped = try out_tmpl.addComponent("trapped");

    _ = try out_trapped.addAttribute("trap", trap.*);
    if (fire_event.*) |event| {
        _ = try out_trapped.addAttribute("fire_event", event);
    }
}

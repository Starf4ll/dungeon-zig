const std = @import("std");
const pow = std.math.pow;
const ContainerAttributes = @import("ContainersMap.zig").ContainerAttributes;

/// Flag controlling if containers are being generated for Depths of Horizon
/// Adds additional item drops for enchantment scrolls
pub const IS_DOH = false;

/// Determines the max damage for a container's trap
pub fn minDamage(container_level: f32, damage_scalar: f32) f64 {
    return 1 + damage_scalar * (0.5 * pow(f64, container_level, 2.0));
}

/// Determines the max damage for a container's trap
pub fn maxDamage(container_level: f32, damage_scalar: f32) f64 {
    return 1 + damage_scalar * (0.6 * pow(f64, container_level, 2.0));
}

/// Determines the minimum power of items dropped by a container
pub fn minItemDropPower(container_level: f32) f64 {
    return 1.9 * pow(f64, container_level, 1.1);
}

/// Determines the maximum power of items dropped by a container
pub fn maxItemDropPower(container_level: f32) f64 {
    return 2.6 * pow(f64, container_level, 1.1);
}

/// Determines the minimum gold dropped by a container
pub fn minGoldDrop(container_level: f32, container_attrs: *const ContainerAttributes) f64 {
    return pow(f64, (0.23 * container_level), 4.0) * container_attrs.gold_mult;
}

/// Determines the maximum gold dropped by a container
pub fn maxGoldDrop(container_level: f32, container_attrs: *const ContainerAttributes) f64 {
    return 2 * pow(f64, (0.23 * container_level), 4.0) * container_attrs.gold_mult;
}

/// Determines the minimum power of spells dropped by a container
pub fn minSpellDropPower(container_level: f32, container_attrs: *const ContainerAttributes) f64 {
    return 0.75 * container_level * container_attrs.spell_mult;
}

/// Determines the maximum power of spells dropped by a container
pub fn maxSpellDropPower(container_level: f32, container_attrs: *const ContainerAttributes) f64 {
    return 1.05 * container_level * container_attrs.spell_mult;
}

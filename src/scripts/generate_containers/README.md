# Generate Containers

A script designed to help automate generation of containers for Dungeon Siege with stats based on formulas.

## Usage

Basic details for each container must be provided in `/scripts/generate_containers/generate_containers.csv`.
This includes the template name, Siege Editor category, level for each world difficulty, as well as the container and trap type.
Values for each column ***must*** be provided; do not leave any columns blank.

The possible container types are defined in `scripts/generate_containers/ContainersMap.zig`. Refer to the Container Map section in this document for details.
For possible trap types, refer to the Trap Types section in this document for details.

Sample values have been provided in the CSV for reference.

---

Once all container details have been prepared in the CSV, run `dungeon_zig.exe` or `zig build run` and choose option 2 from the menu.
If no errors occurred, output templates will be found in `scripts/generate_containers/output/`.

---

Finally, if you wish to modify the formulas used during generation, they can be found in `scripts/generate_containers/GenerateContainersFormulas.zig`.
Changes to the formulas require re-building the application with `zig build run`.

## Container Map

The ContainerMap file holds all of the possible container types that may be used in the CSV.
Each type in the map holds prefined details about a container as well as optional multipliers which will be applied to the formulas used during generation.

A large number of default types are included.
Each default type has three (four in the case of chests) variants; Low, Med and High (and Special for chests).
The variants indicate the multipliers on loot that the container will drop as well as the damage its trap will deal.
The Special variant for chests has significantly higher drop rates for rare and unique items and is intended to be used sparingly.
Special chests include a SFX which is provided in `scripts/generate_containers/chest_special_sfx.gas`. Be sure to include this file in your mod if using special chests.

---

There are variants for all of the following default container types. (i.e. `barrel_cav_01_low`, `barrel_cav_01_med` and `barrel_cav_01_high`).
```
barrel_cav_01       crate_cav_02          footlocker_glb_01     chest_csl_04
barrel_cav_02       crate_cav_03          trunk_glb             chest_glb_02
barrel_csl_web_01   crate_cav_lg          trunk_glb_long        chest_glb_03_skull
barrel_csl_web_02   crate_cav_sm          trunk_glb_golden      chest_glb_03_csl_04
barrel_glb          crate_gob_01                                chest_glb_03_csl_05
barrel_glb_03       crate_gob_02          pot_cav_01            chest_glb_03_glb_01
barrel_glb_04       crate_glb             pot_cav_02            chest_glb_03_ice
barrel_glb_lid      crate_glb_ice         pot_des_01            chest_glb_04
barrel_swp_01       crate_glb_01          pot_des_02            chest_glb_05
                    crate_glb_01_ice      pot_des_03            chest_glb_06_stone
cask_glb            crate_glb_krug        pot_des_04            chest_glb_07
                    crate_glb_krug_02     pot_des_05
                    crate_glb_krug_des    urn_cav_01            locker_glb_01
                    crate_glb_krug_swp    urn_cav_02
                                          vase_glb_01           sarcophagus_cav_01
                                                                sarcophagus_csl_02
                                                                sarcophagus_csl_03
```

## Trap Types

Any trap template which exists in the Dungeon Siege resources (including custom) may be used as a valid trap type.

The default/vanilla trap types are as follows:
```
trp_generator_arrow_barrel                  trp_generator_arrow_crate
trp_generator_explosion_barrel              trp_generator_explosion_crate
trp_generator_fireball_barrel               trp_generator_fireball_crate
trp_generator_fireball_explosive_barrel     trp_generator_fireball_explosive_crate
trp_generator_gas_barrel                    trp_generator_gas_crate
trp_generator_zap_barrel                    trp_generator_zap_crate
trp_generator_star_barrel                   trp_generator_star_crate

trp_generator_arrow_chest
trp_generator_explosion_chest               trp_generator_explosion_urn
trp_generator_fireball_chest                trp_generator_fireball_urn
trp_generator_fireball_explosive_chest      trp_generator_fireball_explosive_urn
trp_generator_flame_chest
trp_generator_frost_chest
trp_generator_gas_chest                     trp_generator_gas_urn
trp_generator_star_chest
trp_generator_zap_chest                     trp_generator_zap_urn

trp_generator_arrow
trp_generator_explosion
trp_generator_fireball
trp_generator_fireball_explosive
trp_generator_firetrap
trp_generator_flame
trp_generator_flames
trp_generator_frost
trp_generator_gas
trp_generator_star
trp_generator_zap

```

const std = @import("std");
const ArrayList = std.ArrayList;
const SegmentedList = std.SegmentedList;
const Allocator = std.mem.Allocator;
const ArenaAllocator = std.heap.ArenaAllocator;
const Timer = std.time.Timer;
const allocPrint = std.fmt.allocPrint;
const pow = std.math.pow;

const Gas = @import("../../gas/Gas.zig");
const GasParser = @import("../../gas/GasParser.zig");
const GasWriter = @import("../../gas/GasWriter.zig");
const Csv = @import("../../csv/Csv.zig");
const Formulas = @import("GenerateMonstersFormulas.zig");

const WORLD_DIFFS = [3][]const u8{ "regular", "veteran", "elite" };
const WORLD_DIFF_PREFIXES = [3][]const u8{ "", "2W_", "3W_" };
const CATEGORY_WORLD_DIFF_PREFIXES = [3][]const u8{ "1W_", "2W_", "3W_" };
const AUTO_MERGE_COMPONENTS = [28][]const u8{
    "body",
    "breaking_object",
    "conversation",
    "common",
    "doppelganger",
    "effect_manager",
    "effect_manager_server",
    "effect_aftereffect",
    "enchantment_manager",
    "follower",
    "guts_manager",
    "magic",
    "mind",
    "physics",
    "attach_robo",
    "alignment_switcher",
    "gom_effects",
    "generator_in_object",
    "self_destruct",
    "tsd_manager",
    "water_effects",
    "dsx_shadow_jumper_heal_monitor",
    "dsx_cicatrix_spawn_monitor",
    "dsx_darkgen_spawn_monitor",
    "dsx_krakbone_spawn_monitor",
    "dsx_krakbone_tentacle_monitor",
    "water_elemental_manager",
    "actor_randomizer",
};

const UPG_STONES = [4][]const u8{ "jewel_upgrade_02", "jewel_upgrade_03", "jewel_upgrade_04", "jewel_upgrade_05" };

pub const MonsterMultipiers = struct {
    level: f32,
    hp: f32,
    defense: f32,
    melee_damage: f32,
    ranged_damage: f32,
    magic_damage: f32,
    damage_variance: f32,
    attack_count: f32,
    xp: f32,
};

/// Generates monsters for Dungeon Siege
/// Reads all Gas files from the input directory, applies various modifications, then writes the modified templates to the output directory
pub fn generateMonsters() !void {
    var total_timer = try Timer.start();

    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    const allocator = gpa.allocator();

    var gas_files = try GasParser.parseDirectory(allocator, "src/scripts/generate_monsters/input");
    var new_gas_files = SegmentedList(Gas.GasFile, 32){};
    const monst_mult_csv = try Csv.CsvParser.parse(allocator, "src/scripts/generate_monsters/generate_monsters.csv", ',');

    defer gas_files.deinit(allocator);
    defer new_gas_files.deinit(allocator);
    defer monst_mult_csv.deinit();

    var it = gas_files.iterator(0);
    while (it.next()) |in_file| {
        for (0..3) |diff_index| {
            const out_file = try new_gas_files.addOne(allocator);
            try generateMonstersInFile(allocator, in_file, out_file, monst_mult_csv, diff_index);
        }
        in_file.deinit();
    }

    std.log.info("Total Time Taken To Generate: {d:.4}ms", .{
        @as(f128, @floatFromInt(total_timer.lap())) / @as(f32, @floatFromInt(std.time.ns_per_ms)),
    });

    try GasWriter.writeAll(allocator, &new_gas_files, "src/scripts/generate_monsters/output");
}

/// Generates new templates for all monsters in a given Gas file
fn generateMonstersInFile(
    allocator: Allocator,
    in_file: *Gas.GasFile,
    out_file: *Gas.GasFile,
    monst_mult_csv: *const Csv.Csv,
    diff_index: usize,
) !void {
    const file_path = try makeWorldDiffFilePath(allocator, in_file, &WORLD_DIFFS[diff_index]);
    const file_name = try allocPrint(allocator, "{s}{s}", .{ WORLD_DIFF_PREFIXES[diff_index], in_file.file_name });
    _ = try Gas.GasFile.init(allocator, out_file, file_path, file_name);
    defer allocator.free(file_path);
    defer allocator.free(file_name);

    var e = in_file.entries.first;
    while (e) |e_node| : (e = e_node.next) {
        var in_tmpl = &e_node.data;
        const is_base_template = std.mem.eql(u8, in_tmpl.name[0..5], "base_");
        if (diff_index != 0 and is_base_template) {
            continue;
        }

        const prefixed_name = try allocPrint(allocator, "{s}{s}", .{ WORLD_DIFF_PREFIXES[diff_index], in_tmpl.name });
        const out_tmpl = try out_file.addEntry(prefixed_name, "template");
        allocator.free(prefixed_name);

        try addBaseAttributes(allocator, in_tmpl, out_tmpl, diff_index);

        if (Formulas.IS_DOH and std.mem.eql(u8, in_tmpl.name[in_tmpl.name.len - 10 ..], "_generator")) {
            try autoMerge(in_tmpl, out_tmpl, "gizmo");
            try autoMerge(in_tmpl, out_tmpl, "monster_respawner");
            continue;
        }

        var monst_mults: ?MonsterMultipiers = null;
        const mob_level_column = 1 + diff_index;
        for (monst_mult_csv.rows) |row| {
            if (std.mem.eql(u8, in_tmpl.name, row[0][1 .. row[0].len - 1])) {
                monst_mults = MonsterMultipiers{
                    .level = try std.fmt.parseFloat(f32, row[mob_level_column]),
                    .hp = try std.fmt.parseFloat(f32, row[4]),
                    .defense = try std.fmt.parseFloat(f32, row[5]),
                    .melee_damage = try std.fmt.parseFloat(f32, row[6]),
                    .ranged_damage = try std.fmt.parseFloat(f32, row[7]),
                    .magic_damage = try std.fmt.parseFloat(f32, row[8]),
                    .damage_variance = try std.fmt.parseFloat(f32, row[9]),
                    .attack_count = try std.fmt.parseFloat(f32, row[10]),
                    .xp = try std.fmt.parseFloat(f32, row[11]),
                };
                break;
            }
        }
        if (monst_mults == null) {
            std.log.warn("Monster not found in generate_monsters.csv: \"{s}\", proceeding with default values", .{in_tmpl.name});
            monst_mults = comptime MonsterMultipiers{
                .level = 1,
                .hp = 1.0,
                .defense = 1.0,
                .melee_damage = 1.0,
                .ranged_damage = 1.0,
                .magic_damage = 1.0,
                .damage_variance = 1.0,
                .attack_count = 1.0,
                .xp = 1.0,
            };
        }
        if (monst_mults.?.level == 0) std.log.warn("Monster is Level 0: \"{s}\"", .{in_tmpl.name});

        var skill_classes: ArrayList([]const u8) = try ArrayList([]const u8).initCapacity(allocator, 4);

        try addActorComponent(allocator, in_tmpl, out_tmpl, &skill_classes, &monst_mults.?);
        try addAspectComponent(allocator, in_tmpl, out_tmpl, &skill_classes, &monst_mults.?, Formulas.DIFF_MULTS[diff_index], &is_base_template);
        try addAttackComponent(allocator, in_tmpl, out_tmpl, &skill_classes, &monst_mults.?, Formulas.DIFF_MULTS[diff_index], &is_base_template);
        for (AUTO_MERGE_COMPONENTS[0..4]) |c| {
            try autoMerge(in_tmpl, out_tmpl, c);
        }
        try addDefenseComponent(allocator, in_tmpl, out_tmpl, &monst_mults.?, Formulas.DIFF_MULTS[diff_index], &is_base_template);
        for (AUTO_MERGE_COMPONENTS[4..11]) |c| {
            try autoMerge(in_tmpl, out_tmpl, c);
        }
        try addInventoryComponent(allocator, in_tmpl, out_tmpl, &monst_mults.?, &is_base_template, &UPG_STONES[diff_index], &UPG_STONES[diff_index + 1]);
        for (AUTO_MERGE_COMPONENTS[11..]) |c| {
            try autoMerge(in_tmpl, out_tmpl, c);
        }
    }
}

/// Copies over all data for a given Component from the input template to the output template
fn autoMerge(in_templ: *Gas.GasEntry, out_tmpl: *Gas.GasEntry, component_name: []const u8) !void {
    if (in_templ.getComponent(component_name)) |in_component| {
        const out_component = try out_tmpl.addComponent(component_name);
        try in_component.merge(out_component, true);
    }
}

/// Determines the output file path based on the current world difficulty
fn makeWorldDiffFilePath(
    allocator: Allocator,
    in_file: *Gas.GasFile,
    world_diff: *const []const u8,
) ![]const u8 {
    var buffer: ArrayList(u8) = try ArrayList(u8).initCapacity(allocator, 21);
    try buffer.appendSlice(world_diff.*);
    try buffer.appendSlice(blk: {
        var i: u8 = 0;
        while (i < in_file.file_path.len) : (i += 1) {
            if (in_file.file_path[i] == '\\') {
                break;
            }
        }
        break :blk in_file.file_path[i..];
    });
    return try buffer.toOwnedSlice();
}

/// Adds the base level Attributes for the given template
fn addBaseAttributes(
    allocator: Allocator,
    in_tmpl: *Gas.GasEntry,
    out_tmpl: *Gas.GasEntry,
    diff_index: usize,
) !void {
    if (in_tmpl.getAttribute("category_name")) |category_name| {
        const category_name_value = try allocPrint(allocator, "\"{s}{s}", .{ CATEGORY_WORLD_DIFF_PREFIXES[diff_index], category_name.value[4..] });
        _ = try out_tmpl.addAttribute(category_name.name, category_name_value);
        allocator.free(category_name_value);
    }
    if (in_tmpl.getAttribute("doc")) |doc| {
        const doc_value = try allocPrint(allocator, "\"{s}{s}", .{ WORLD_DIFF_PREFIXES[diff_index], doc.value[1..] });
        _ = try out_tmpl.addAttribute(doc.name, doc_value);
        allocator.free(doc_value);
    }
    if (in_tmpl.getAttribute("specializes")) |specializes| {
        _ = try out_tmpl.addAttribute(specializes.name, specializes.value);
    }
}

/// Adds the Actor Component for the given template
/// Skill levels are determined by the monsters level in generate_monsters.csv
/// Stats are based on the monster's skills, multipliers from generate_monsters.csv, and the formulas in GenerateMonstersFormulas
/// Other Attributes / Sub-Components are copied over
fn addActorComponent(
    allocator: Allocator,
    in_tmpl: *Gas.GasEntry,
    out_tmpl: *Gas.GasEntry,
    skill_classes: *ArrayList([]const u8),
    monst_mults: *MonsterMultipiers,
) !void {
    var melee_skill_delta: f32 = 1.0;
    var ranged_skill_delta: f32 = 1.0;
    var combat_skill_delta: f32 = 1.0;
    if (in_tmpl.getComponent("actor")) |in_actor| {
        if (in_actor.isEmpty()) return;

        var out_actor = try out_tmpl.addComponent(in_actor.name);
        if (in_actor.getComponent("skills")) |in_skills| {
            var out_skills = try out_actor.addComponent(in_skills.name);
            try getSetSkillAttributes(allocator, in_skills, out_skills, skill_classes, monst_mults, &melee_skill_delta, "melee");
            try getSetSkillAttributes(allocator, in_skills, out_skills, skill_classes, monst_mults, &ranged_skill_delta, "ranged");
            try getSetSkillAttributes(allocator, in_skills, out_skills, skill_classes, monst_mults, &combat_skill_delta, "combat_magic");

            var strength: f32 = undefined;
            var dexterity: f32 = undefined;
            var intelligence: f32 = undefined;

            if (in_skills.getAttribute("melee")) |_| {
                strength = Formulas.strengthIfMelee(monst_mults);
            } else {
                strength = Formulas.strength(monst_mults);
            }

            const es_weapon_hand: ?[]const u8 = lookupEquipment(in_tmpl, "es_weapon_hand");
            if (ranged_skill_delta != 1.0 or (es_weapon_hand == null and combat_skill_delta == 1.0)) {
                dexterity = Formulas.dexterityIfRangedOrBeastfu(monst_mults);
            } else {
                dexterity = Formulas.dexterity(monst_mults);
            }

            if (combat_skill_delta != 1.0) {
                intelligence = Formulas.intelligenceIfCombatMagic(monst_mults);
            } else {
                intelligence = Formulas.intelligence(monst_mults);
            }

            const strength_str = try allocPrint(allocator, "{d:.1}, 0", .{strength});
            const dexterity_str = try allocPrint(allocator, "{d:.1}, 0", .{dexterity});
            const intelligence_str = try allocPrint(allocator, "{d:.1}, 0", .{intelligence});

            _ = try out_skills.addAttribute("strength", strength_str);
            _ = try out_skills.addAttribute("dexterity", dexterity_str);
            _ = try out_skills.addAttribute("intelligence", intelligence_str);

            allocator.free(strength_str);
            allocator.free(dexterity_str);
            allocator.free(intelligence_str);
        }
        try in_actor.merge(out_actor, false);
    }
}

/// Adds the given skill Attribute to a monster
/// Also checks the difference between the monster's original skill level and their new skill level
fn getSetSkillAttributes(
    allocator: Allocator,
    in_skills: *Gas.Component,
    out_skills: *Gas.Component,
    skill_classes: *ArrayList([]const u8),
    monst_mults: *MonsterMultipiers,
    skill_delta: *f32,
    attribute: []const u8,
) !void {
    if (in_skills.getAttribute(attribute)) |attr| {
        try skill_classes.append(attribute);

        var i: usize = 0;
        while (i < attr.value.len) : (i += 1) {
            if (attr.value[i] == ',') break;
        }
        var skill_level: f32 = try std.fmt.parseFloat(f32, attr.value[0..i]);

        if (monst_mults.level != 0.0) {
            skill_delta.* = skill_level / monst_mults.level;
            skill_level = monst_mults.level;
        }

        const skill_level_str = try allocPrint(allocator, "{d:.1}, 0", .{skill_level});
        _ = try out_skills.addAttribute(attribute, skill_level_str);
        allocator.free(skill_level_str);
    }
}

/// Checks if a monster has an equipped weapon in the given equip slot
/// We have no idea which subcomponent the equipped weapon might be in, so gotta check em all
fn lookupEquipment(
    in_tmpl: *Gas.GasEntry,
    equip_slot: []const u8,
) ?[]const u8 {
    if (in_tmpl.getComponent("inventory")) |inventory| {
        // [equipment]
        if (inventory.getComponent("equipment")) |equipment| {
            if (equipment.getAttribute(equip_slot)) |es| {
                return es.value;
            }
        }
        // [pcontent]
        if (inventory.getComponent("pcontent")) |pcontent| {
            if (pcontent.getAttribute(equip_slot)) |es| {
                return es.value;
            }
            // [pcontent][oneof*]
            if (pcontent.getComponent("oneof*")) |oneof| {
                if (oneof.getAttribute(equip_slot)) |es| {
                    return es.value;
                }
                // [pcontent][oneof*][all*]
                if (oneof.getComponent("all*")) |all| {
                    if (all.getAttribute(equip_slot)) |es| {
                        return es.value;
                    }
                }
            }
            // [pcontent][all*]
            if (pcontent.getComponent("all*")) |all| {
                if (all.getAttribute(equip_slot)) |es| {
                    return es.value;
                }
                // [pcontent][all*][oneof*]
                if (all.getComponent("oneof*")) |oneof| {
                    if (oneof.getAttribute(equip_slot)) |es| {
                        return es.value;
                    }
                }
            }
        }
    }
    return null;
}

/// Adds the Aspect Component for the given template
/// Life, Mana and XP are based on current world difficulty, multipliers from generate_monsters.csv, and the formulas in GenerateMonstersFormulas
/// Other Attributes / Sub-Components are copied over
fn addAspectComponent(
    allocator: Allocator,
    in_tmpl: *Gas.GasEntry,
    out_tmpl: *Gas.GasEntry,
    skill_classes: *ArrayList([]const u8),
    monst_mults: *MonsterMultipiers,
    diff_mult: f32,
    is_base_template: *const bool,
) !void {
    var out_aspect = try out_tmpl.addComponent("aspect");

    if (!is_base_template.*) {
        const life = Formulas.life(monst_mults, diff_mult);
        const life_str = try allocPrint(allocator, "{d}", .{@round(life)});
        _ = try out_aspect.addAttribute("life", life_str);
        _ = try out_aspect.addAttribute("max_life", life_str);
        allocator.free(life_str);

        var has_combat: bool = false;
        for (skill_classes.items) |class| {
            if (std.mem.eql(u8, class, "combat_magic")) {
                has_combat = true;
                break;
            }
        }
        const mana = Formulas.mana(monst_mults, diff_mult, has_combat);
        const mana_str = try allocPrint(allocator, "{d}", .{@round(mana)});
        _ = try out_aspect.addAttribute("mana", mana_str);
        _ = try out_aspect.addAttribute("max_mana", mana_str);
        allocator.free(mana_str);

        const xp = Formulas.xp(monst_mults, diff_mult);
        const xp_str = try allocPrint(allocator, "{d}", .{@round(xp)});
        _ = try out_aspect.addAttribute("experience_value", xp_str);
        allocator.free(xp_str);
    }
    if (in_tmpl.getComponent("aspect")) |in_aspect| {
        try in_aspect.merge(out_aspect, false);
    }
}

/// Adds the Attack Component for the given template
/// Damage is based on the current world difficulty, the monster's skills, multipliers from generate_monsters.csv, and the formulas in GenerateMonstersFormulas
/// Other Attributes / Sub-Components are copied over
fn addAttackComponent(
    allocator: Allocator,
    in_tmpl: *Gas.GasEntry,
    out_tmpl: *Gas.GasEntry,
    skill_classes: *ArrayList([]const u8),
    monst_mults: *MonsterMultipiers,
    diff_mult: f32,
    is_base_template: *const bool,
) !void {
    if (in_tmpl.getComponent("attack")) |in_attack| {
        if (in_attack.isEmpty()) return;

        var out_attack = try out_tmpl.addComponent("attack");

        if (!is_base_template.*) {
            const avg_melee_dmg = Formulas.averageMeleeDamage(monst_mults, diff_mult);
            const avg_ranged_dmg = Formulas.averageRangedDamage(monst_mults, diff_mult);
            const avg_magic_dmg = Formulas.averageMagicDamage(monst_mults, diff_mult);
            const min_variance = Formulas.minDamageVariance(monst_mults);
            const max_variance = Formulas.maxDamageVariance(monst_mults);

            for (skill_classes.items) |class| {
                if (std.mem.eql(u8, class, "melee")) {
                    const min_melee_dmg = try allocPrint(allocator, "{d}", .{@round(avg_melee_dmg * min_variance)});
                    const max_melee_dmg = try allocPrint(allocator, "{d}", .{@round(avg_melee_dmg * max_variance)});
                    _ = try out_attack.addAttribute("damage_bonus_min_melee", min_melee_dmg);
                    _ = try out_attack.addAttribute("damage_bonus_max_melee", max_melee_dmg);
                    allocator.free(min_melee_dmg);
                    allocator.free(max_melee_dmg);
                }
                if (std.mem.eql(u8, class, "ranged")) {
                    const min_ranged_dmg = try allocPrint(allocator, "{d}", .{@round(avg_ranged_dmg * min_variance)});
                    const max_ranged_dmg = try allocPrint(allocator, "{d}", .{@round(avg_ranged_dmg * max_variance)});
                    _ = try out_attack.addAttribute("damage_bonus_min_ranged", min_ranged_dmg);
                    _ = try out_attack.addAttribute("damage_bonus_max_ranged", max_ranged_dmg);
                    allocator.free(min_ranged_dmg);
                    allocator.free(max_ranged_dmg);
                }
                if (std.mem.eql(u8, class, "combat_magic")) {
                    const min_magic_dmg = try allocPrint(allocator, "{d}", .{@round(avg_magic_dmg * min_variance)});
                    const max_magic_dmg = try allocPrint(allocator, "{d}", .{@round(avg_magic_dmg * max_variance)});
                    _ = try out_attack.addAttribute("damage_bonus_min_cmagic", min_magic_dmg);
                    _ = try out_attack.addAttribute("damage_bonus_max_cmagic", max_magic_dmg);
                    allocator.free(min_magic_dmg);
                    allocator.free(max_magic_dmg);
                }
            }
        }

        try in_attack.merge(out_attack, false);
    }
}

/// Adds the Defense Component for the given template
/// Defense is based on current world difficulty, multipliers from generate_monsters.csv, and the formulas in GenerateMonstersFormulas
/// Other Attributes / Sub-Components are copied over
fn addDefenseComponent(
    allocator: Allocator,
    in_tmpl: *Gas.GasEntry,
    out_tmpl: *Gas.GasEntry,
    monst_mults: *MonsterMultipiers,
    diff_mult: f32,
    is_base_template: *const bool,
) !void {
    var out_defend: ?*Gas.Component = null;

    if (!is_base_template.*) {
        out_defend = try out_tmpl.addComponent("defend");

        const defense = Formulas.defense(monst_mults, diff_mult);
        const defense_str = try allocPrint(allocator, "{d}", .{@round(defense)});
        _ = try out_defend.?.addAttribute("defense", defense_str);
        allocator.free(defense_str);
    }
    if (in_tmpl.getComponent("defend")) |in_defend| {
        if (out_defend == null) {
            out_defend = try out_tmpl.addComponent("defend");
        }
        try in_defend.merge(out_defend.?, false);
    }
}

/// Adds the Inventory Component for the given template
/// Item drops are based on multipliers from generate_monsters.csv and the formulas in GenerateMonstersFormulas
/// Other Attributes / Sub-Components are copied over
fn addInventoryComponent(
    allocator: Allocator,
    in_tmpl: *Gas.GasEntry,
    out_tmpl: *Gas.GasEntry,
    monst_mults: *MonsterMultipiers,
    is_base_template: *const bool,
    stone_template_common: *const []const u8,
    stone_template_rare: *const []const u8,
) !void {
    if (is_base_template.*) return;

    var out_inventory = try out_tmpl.addComponent("inventory");

    if (in_tmpl.getComponent("inventory")) |in_inventory| {
        try in_inventory.merge(out_inventory, true);
    }

    // Don't add droppable items to summons
    var w1: usize = 0;
    var w2: usize = 6;
    while (w2 <= in_tmpl.name.len) : ({
        w1 += 1;
        w2 += 1;
    }) {
        if (std.mem.eql(u8, in_tmpl.name[w1..w2], "summon")) {
            return;
        }
    }

    var out_pcontent = out_inventory.getComponent("pcontent") orelse try out_inventory.addComponent("pcontent");

    const min_item_power = Formulas.minItemDropPower(monst_mults);
    const max_item_power = Formulas.maxItemDropPower(monst_mults);
    const min_gold = Formulas.minGoldDrop(monst_mults);
    const max_gold = Formulas.maxGoldDrop(monst_mults);

    const armor_power_str = try allocPrint(allocator, "{d}-{d}", .{ @round(min_item_power * 2.2), @round(max_item_power * 2.2) });
    const item_power_str = try allocPrint(allocator, "{d}-{d}", .{ @round(min_item_power), @round(max_item_power) });
    const spell_power_str = try allocPrint(allocator, "{d}-{d}", .{ @round(Formulas.minSpellDropPower(monst_mults)), @round(Formulas.maxSpellDropPower(monst_mults)) });

    const one_of_gold_min_str = try allocPrint(allocator, "{d}", .{@round(min_gold)});
    const one_of_gold_max_str = try allocPrint(allocator, "{d}", .{@round(max_gold)});

    const spell_value_str = try allocPrint(allocator, "#spell/{s}", .{spell_power_str});
    const common_armor_value_str = try allocPrint(allocator, "#armor/{s}", .{armor_power_str});
    const common_weapon_value_str = try allocPrint(allocator, "#weapon/{s}", .{item_power_str});
    const common_item_value_str = try allocPrint(allocator, "#*/{s}", .{item_power_str});
    const rare_1_armor_value_str = try allocPrint(allocator, "#armor/-rare(1)/{s}", .{armor_power_str});
    const rare_1_weapon_value_str = try allocPrint(allocator, "#weapon/-rare(1)/{s}", .{item_power_str});
    const rare_1_item_value_str = try allocPrint(allocator, "#*/-rare(1)/{s}", .{item_power_str});
    const rare_2_armor_value_str = try allocPrint(allocator, "#armor/-rare(2)/{s}", .{armor_power_str});
    const rare_2_weapon_value_str = try allocPrint(allocator, "#weapon/-rare(2)/{s}", .{item_power_str});
    const rare_2_item_value_str = try allocPrint(allocator, "#*/-rare(2)/{s}", .{item_power_str});
    const unique_1_armor_value_str = try allocPrint(allocator, "#armor/-unique(1)/{s}", .{armor_power_str});
    const unique_1_weapon_value_str = try allocPrint(allocator, "#weapon/-unique(1)/{s}", .{item_power_str});
    const unique_1_item_value_str = try allocPrint(allocator, "#*/-unique(1)/{s}", .{item_power_str});
    const unique_2_armor_value_str = try allocPrint(allocator, "#armor/-unique(2)/{s}", .{armor_power_str});
    const unique_2_weapon_value_str = try allocPrint(allocator, "#weapon/-unique(2)/{s}", .{item_power_str});
    const unique_2_item_value_str = try allocPrint(allocator, "#*/-unique(2)/{s}", .{item_power_str});

    defer allocator.free(armor_power_str);
    defer allocator.free(item_power_str);
    defer allocator.free(spell_power_str);
    defer allocator.free(one_of_gold_min_str);
    defer allocator.free(one_of_gold_max_str);
    defer allocator.free(spell_value_str);
    defer allocator.free(common_armor_value_str);
    defer allocator.free(common_weapon_value_str);
    defer allocator.free(common_item_value_str);
    defer allocator.free(rare_1_armor_value_str);
    defer allocator.free(rare_1_weapon_value_str);
    defer allocator.free(rare_1_item_value_str);
    defer allocator.free(rare_2_armor_value_str);
    defer allocator.free(rare_2_weapon_value_str);
    defer allocator.free(rare_2_item_value_str);
    defer allocator.free(unique_1_armor_value_str);
    defer allocator.free(unique_1_weapon_value_str);
    defer allocator.free(unique_1_item_value_str);
    defer allocator.free(unique_2_armor_value_str);
    defer allocator.free(unique_2_weapon_value_str);
    defer allocator.free(unique_2_item_value_str);

    var gold_and_potions = try out_pcontent.addComponent("all*");
    var one_of_gold = try gold_and_potions.addComponent("gold*");
    _ = try one_of_gold.addAttribute("chance", Formulas.goldDropChance());
    _ = try one_of_gold.addAttribute("min", one_of_gold_min_str);
    _ = try one_of_gold.addAttribute("max", one_of_gold_max_str);
    var one_of_potion = try gold_and_potions.addComponent("oneof*");
    _ = try one_of_potion.addAttribute("chance", Formulas.potionDropChance());
    _ = try one_of_potion.addAttribute("il_main", "potion_health_super");
    _ = try one_of_potion.addAttribute("il_main", "potion_mana_super");

    var items = try out_pcontent.addComponent("oneof*");
    var spells = try items.addComponent("oneof*");
    _ = try spells.addAttribute("chance", Formulas.spellDropChance());
    _ = try spells.addAttribute("il_main", spell_value_str);
    var common_items = try items.addComponent("oneof*");
    _ = try common_items.addAttribute("chance", Formulas.commonItemDropChance());
    _ = try common_items.addAttribute("il_main", common_armor_value_str);
    _ = try common_items.addAttribute("il_main", common_weapon_value_str);
    _ = try common_items.addAttribute("il_main", common_item_value_str);
    var rare_1_items = try items.addComponent("oneof*");
    _ = try rare_1_items.addAttribute("chance", Formulas.rare1ItemDropChance());
    _ = try rare_1_items.addAttribute("il_main", rare_1_armor_value_str);
    _ = try rare_1_items.addAttribute("il_main", rare_1_weapon_value_str);
    _ = try rare_1_items.addAttribute("il_main", rare_1_item_value_str);
    var rare_2_items = try items.addComponent("oneof*");
    _ = try rare_2_items.addAttribute("chance", Formulas.rare2ItemDropChance());
    _ = try rare_2_items.addAttribute("il_main", rare_2_armor_value_str);
    _ = try rare_2_items.addAttribute("il_main", rare_2_weapon_value_str);
    _ = try rare_2_items.addAttribute("il_main", rare_2_item_value_str);
    var unique_1_items = try items.addComponent("oneof*");
    _ = try unique_1_items.addAttribute("chance", Formulas.unique1ItemDropChance());
    _ = try unique_1_items.addAttribute("il_main", unique_1_armor_value_str);
    _ = try unique_1_items.addAttribute("il_main", unique_1_weapon_value_str);
    _ = try unique_1_items.addAttribute("il_main", unique_1_item_value_str);
    var unique_2_items = try items.addComponent("oneof*");
    _ = try unique_2_items.addAttribute("chance", Formulas.unique2ItemDropChance());
    _ = try unique_2_items.addAttribute("il_main", unique_2_armor_value_str);
    _ = try unique_2_items.addAttribute("il_main", unique_2_weapon_value_str);
    _ = try unique_2_items.addAttribute("il_main", unique_2_item_value_str);

    if (Formulas.IS_DOH) {
        try addCustomInventoryItems(allocator, out_pcontent, &monst_mults.level, stone_template_common, stone_template_rare);
    }
}

/// Adds custom item drops for Depth Of Horizon monsters
fn addCustomInventoryItems(
    allocator: Allocator,
    out_pcontent: *Gas.Component,
    monster_level: *f32,
    stone_template_common: *const []const u8,
    stone_template_rare: *const []const u8,
) !void {
    var scroll_power = @ceil(1 + (0.0029 * pow(f32, monster_level.*, 1.9)) + (1.65 * pow(f32, monster_level.*, 0.75)));
    if (scroll_power > 190) scroll_power = 190;
    const scroll_min_power = @ceil(scroll_power / 1.6);

    const common_stone_chance = try allocPrint(allocator, "{d:.4}", .{0.01 + (0.0002 * pow(f32, monster_level.*, 0.59))});
    const rare_stone_chance = try allocPrint(allocator, "{d:.4}", .{0.002 + (0.0002 * pow(f32, monster_level.*, 0.67))});

    const uncommon_scroll_chance = try allocPrint(allocator, "{d:.4}", .{0.0028 + (0.000007 * monster_level.*)});
    const rare_scroll_chance = try allocPrint(allocator, "{d:.4}", .{0.0016 + (0.000007 * monster_level.*)});
    const unique_scroll_chance = try allocPrint(allocator, "{d:.4}", .{0.001 + (0.000007 * monster_level.*)});

    const magic_scroll_value = try allocPrint(allocator, "#scroll/{d}-{d}", .{ 1000 + scroll_min_power, 1000 + scroll_power });
    const imbued_scroll_value = try allocPrint(allocator, "#scroll/{d}-{d}", .{ 1200 + scroll_min_power, 1200 + scroll_power });
    const negative_scroll_value = try allocPrint(allocator, "#scroll/{d}-{d}", .{ 1600 + scroll_min_power, 1600 + scroll_power });
    const uncommon_scroll_value = try allocPrint(allocator, "#scroll/{d}-{d}", .{ 1400 + scroll_min_power, 1400 + scroll_power });
    const rare_scroll_value = try allocPrint(allocator, "#scroll/{d}-{d}", .{ 1800 + scroll_min_power, 1800 + scroll_power });
    const unique_scroll_value = try allocPrint(allocator, "#scroll/{d}-{d}", .{ 2200 + scroll_min_power, 2200 + scroll_power });
    const runic_scroll_value = try allocPrint(allocator, "#scroll/{d}-{d}", .{ 2000 + scroll_min_power, 2000 + scroll_power });
    const legendary_scroll_value = try allocPrint(allocator, "#scroll/{d}-{d}", .{ 2400 + scroll_min_power, 2400 + scroll_power });
    const artifact_scroll_value = try allocPrint(allocator, "#scroll/{d}-{d}", .{ 2600 + scroll_min_power, 2600 + scroll_power });

    defer allocator.free(common_stone_chance);
    defer allocator.free(rare_stone_chance);
    defer allocator.free(uncommon_scroll_chance);
    defer allocator.free(rare_scroll_chance);
    defer allocator.free(unique_scroll_chance);
    defer allocator.free(magic_scroll_value);
    defer allocator.free(imbued_scroll_value);
    defer allocator.free(negative_scroll_value);
    defer allocator.free(uncommon_scroll_value);
    defer allocator.free(rare_scroll_value);
    defer allocator.free(unique_scroll_value);
    defer allocator.free(runic_scroll_value);
    defer allocator.free(legendary_scroll_value);
    defer allocator.free(artifact_scroll_value);

    // Add upgrade stones
    var one_of_stone = try out_pcontent.addComponent("oneof*");
    var one_of_common_stone = try one_of_stone.addComponent("oneof*");
    _ = try one_of_common_stone.addAttribute("chance", common_stone_chance);
    _ = try one_of_common_stone.addAttribute("il_main", stone_template_common.*);
    var one_of_rare_stone = try one_of_stone.addComponent("oneof*");
    _ = try one_of_rare_stone.addAttribute("chance", rare_stone_chance);
    _ = try one_of_rare_stone.addAttribute("il_main", stone_template_rare.*);

    // Add enchantment scrolls
    var one_of_scroll = try out_pcontent.addComponent("oneof*");
    var one_of_magic_scroll = try one_of_scroll.addComponent("oneof*");
    _ = try one_of_magic_scroll.addAttribute("chance", "0.0065");
    _ = try one_of_magic_scroll.addAttribute("il_main", magic_scroll_value);
    var one_of_imbued_or_negative_scroll = try one_of_scroll.addComponent("oneof*");
    _ = try one_of_imbued_or_negative_scroll.addAttribute("chance", "0.0035");
    _ = try one_of_imbued_or_negative_scroll.addAttribute("il_main", imbued_scroll_value);
    _ = try one_of_imbued_or_negative_scroll.addAttribute("il_main", negative_scroll_value);
    var one_of_uncommon_scroll = try one_of_scroll.addComponent("oneof*");
    _ = try one_of_uncommon_scroll.addAttribute("chance", uncommon_scroll_chance);
    _ = try one_of_uncommon_scroll.addAttribute("il_main", uncommon_scroll_value);
    var one_of_rare_scroll = try one_of_scroll.addComponent("oneof*");
    _ = try one_of_rare_scroll.addAttribute("chance", rare_scroll_chance);
    _ = try one_of_rare_scroll.addAttribute("il_main", rare_scroll_value);
    var one_of_unique_scroll = try one_of_scroll.addComponent("oneof*");
    _ = try one_of_unique_scroll.addAttribute("chance", unique_scroll_chance);
    _ = try one_of_unique_scroll.addAttribute("il_main", unique_scroll_value);
    if (scroll_power > 45) {
        var one_of_runic_or_legendary_scroll = try one_of_scroll.addComponent("oneof*");
        _ = try one_of_runic_or_legendary_scroll.addAttribute("chance", "0.0005");
        _ = try one_of_runic_or_legendary_scroll.addAttribute("il_main", runic_scroll_value);
        _ = try one_of_runic_or_legendary_scroll.addAttribute("il_main", legendary_scroll_value);
        if (scroll_power > 80) {
            var one_of_artifact_scroll = try one_of_scroll.addComponent("oneof*");
            _ = try one_of_artifact_scroll.addAttribute("chance", "0.0002");
            _ = try one_of_artifact_scroll.addAttribute("il_main", artifact_scroll_value);
        }
    }
}

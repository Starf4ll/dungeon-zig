const std = @import("std");
const pow = std.math.pow;
const MonsterMultipiers = @import("GenerateMonsters.zig").MonsterMultipiers;

/// Multipliers to certain formulas based on the currently world difficulty (Regular/Veteran/Elite)
pub const DIFF_MULTS = [3]f32{ 1.0, 0.9, 0.8 };

/// Flag controlling if monsters are being generated for Depths of Horizon
/// Adds additional item drops for upgrade stones and enchantment scrolls
pub const IS_DOH = false;

/// Determines a monster's Strength value if they have Melee Skill
pub fn strengthIfMelee(monst_mults: *MonsterMultipiers) f32 {
    return 9.0 + pow(f32, monst_mults.level, 0.93);
}

/// Determines a monster's default Strength value
pub fn strength(monst_mults: *MonsterMultipiers) f32 {
    return 9.0 + pow(f32, monst_mults.level, 0.89);
}

/// Determines a monster's Dexterity value if they have Ranged Skill, or Melee Skill and are unarmed (beastfu)
pub fn dexterityIfRangedOrBeastfu(monst_mults: *MonsterMultipiers) f32 {
    return monst_mults.level * 0.94;
}

/// Determines a monster's default Dexterity value
pub fn dexterity(monst_mults: *MonsterMultipiers) f32 {
    return 9.0 + pow(f32, monst_mults.level, 0.82);
}

/// Determines a monster's Intelligence value if they have Combat Magic Skill
pub fn intelligenceIfCombatMagic(monst_mults: *MonsterMultipiers) f32 {
    return 9.0 + pow(f32, monst_mults.level, 0.96);
}

/// Determines a monster's default Intelligence value
pub fn intelligence(monst_mults: *MonsterMultipiers) f32 {
    return 9.0 + pow(f32, monst_mults.level, 0.75);
}

/// Determines a monster's HP/Life value
pub fn life(monst_mults: *MonsterMultipiers, diff_mult: f32) f64 {
    return 1 + ((0.2 * pow(f64, monst_mults.level, 2.23)) * monst_mults.hp * diff_mult);
}

/// Determines a monster's MP/Mana value
/// Monsters without combat magic have a reduced value
pub fn mana(monst_mults: *MonsterMultipiers, diff_mult: f32, has_combat_magic: bool) f64 {
    var mana_value: f64 = 1 + ((0.2 * pow(f64, monst_mults.level, 2.23)) * monst_mults.hp * diff_mult);
    if (!has_combat_magic) mana_value *= 0.5;
    return mana_value;
}

/// Determines a monster's XP value
pub fn xp(monst_mults: *MonsterMultipiers, diff_mult: f32) f64 {
    return (150 * pow(
        f64,
        3.0,
        (0.1 * monst_mults.level),
    )) * pow(
        f64,
        (monst_mults.hp * monst_mults.defense * monst_mults.melee_damage * monst_mults.ranged_damage * monst_mults.magic_damage),
        0.5,
    ) * monst_mults.xp * diff_mult;
}

/// Determines a monster's average damage
/// Not used directly, it is instead plugged into the formulas for Melee/Ranged/Magic damage
pub fn averageDamage(monst_mults: *MonsterMultipiers, diff_mult: f32) f64 {
    return 1 + (((0.15 * pow(f64, monst_mults.level, 2.0)) * diff_mult) / monst_mults.attack_count);
}

/// Determines a monster's average Melee damage
pub fn averageMeleeDamage(monst_mults: *MonsterMultipiers, diff_mult: f32) f64 {
    return monst_mults.melee_damage * averageDamage(monst_mults, diff_mult);
}

/// Determines a monster's average Ranged damage
pub fn averageRangedDamage(monst_mults: *MonsterMultipiers, diff_mult: f32) f64 {
    return monst_mults.ranged_damage * averageDamage(monst_mults, diff_mult);
}

/// Determines a monster's average Magic damage
pub fn averageMagicDamage(monst_mults: *MonsterMultipiers, diff_mult: f32) f64 {
    return monst_mults.magic_damage * averageDamage(monst_mults, diff_mult);
}

/// Determines a monster's minimum damage variance
pub fn minDamageVariance(monst_mults: *MonsterMultipiers) f64 {
    return 1 - (0.2 * monst_mults.damage_variance);
}

/// Determines a monster's maximum damage variance
pub fn maxDamageVariance(monst_mults: *MonsterMultipiers) f64 {
    return 1 + (0.2 * monst_mults.damage_variance);
}

/// Determines a monster's Defense value
pub fn defense(monst_mults: *MonsterMultipiers, diff_mult: f32) f64 {
    return 1 + ((0.1 * pow(f64, monst_mults.level, 2.05)) * monst_mults.defense * diff_mult);
}

/// Determines the multiplier to the power of items dropped by monsters
/// Not used directly, it is instead plugged into the formulas for min/max item and gold power
pub fn itemDropPowerMultiplier(monst_mults: *MonsterMultipiers) f64 {
    return pow(
        f64,
        (monst_mults.hp * monst_mults.defense * monst_mults.melee_damage * monst_mults.ranged_damage * monst_mults.magic_damage),
        0.1,
    );
}

/// Determines the minimum power of items dropped by a monster
pub fn minItemDropPower(monst_mults: *MonsterMultipiers) f64 {
    return 1.9 * pow(f64, monst_mults.level, 1.1) * itemDropPowerMultiplier(monst_mults);
}

/// Determines the maximum power of items dropped by a monster
pub fn maxItemDropPower(monst_mults: *MonsterMultipiers) f64 {
    return 2.6 * pow(f64, monst_mults.level, 1.1) * itemDropPowerMultiplier(monst_mults);
}

/// Determines the minimum gold dropped by a monster
pub fn minGoldDrop(monst_mults: *MonsterMultipiers) f64 {
    return pow(f64, (0.23 * monst_mults.level), 4.0) * itemDropPowerMultiplier(monst_mults);
}

/// Determines the maximum gold dropped by a monster
pub fn maxGoldDrop(monst_mults: *MonsterMultipiers) f64 {
    return 2 * pow(f64, (0.23 * monst_mults.level), 4.0) * itemDropPowerMultiplier(monst_mults);
}

/// Determines the minimum power of spells dropped by a monster
pub fn minSpellDropPower(monst_mults: *MonsterMultipiers) f64 {
    return monst_mults.level * 0.75;
}

/// Determines the maximum power of spells dropped by a monster
pub fn maxSpellDropPower(monst_mults: *MonsterMultipiers) f64 {
    return monst_mults.level * 1.05;
}

/// Determines the chance for a monster to drop gold
pub fn goldDropChance() []const u8 {
    return "0.1";
}

/// Determines the chance for a monster to drop potions
pub fn potionDropChance() []const u8 {
    return "0.08";
}

/// Determines the chance for a monster to drop spells
pub fn spellDropChance() []const u8 {
    return "0.025";
}

/// Determines the chance for a monster to drop common items
pub fn commonItemDropChance() []const u8 {
    return "0.06";
}

/// Determines the chance for a monster to drop a rare item with 1 affix
pub fn rare1ItemDropChance() []const u8 {
    return "0.0035";
}

/// Determines the chance for a monster to drop a rare item with 2 affixes
pub fn rare2ItemDropChance() []const u8 {
    return "0.0015";
}

/// Determines the chance for a monster to drop a unique item with 1 affix
pub fn unique1ItemDropChance() []const u8 {
    return "0.001";
}

/// Determines the chance for a monster to drop a unique item with 2 affixes
pub fn unique2ItemDropChance() []const u8 {
    return "0.0005";
}

# Generate Monsters

A script designed to help automate generation of monsters for Dungeon Siege with stats based on formulas.

## Usage

First, input templates must be created in `scripts/generate_monsters/input/`.
This is because although this script will add certain Components and Attributes (such as Aspect, Attack and Defend), the requirements of your specific monsters cannot be fully known.
Therefore an outline of what they contain must first be defined.

It is recommended to separate each unique monster into a Base template and an Instance template.
The Base template is responsible for defining Components and Attributes which are generally immutable, such as `Body` or `Mind`.
The Instance template should have placeholders for the Components and Attributes which will be modified by the script.

A sample of this structure is provided below. This file is also included with the source for reference.

```
// Base Template
[t:template,n:base_doh_gremlin]
{
    doc = "base_doh_gremlin";
    specializes = base_goblin;
    [aspect]
    {
        model = m_c_eam_ggt_pos_1;
        scale_multiplier = 0.65;
        [textures]
        {
            0 = b_c_eam_gremlin;
        }
    }
    [body]
    {
        min_move_velocity = 2.30;
        avg_move_velocity = 3.70;
	}
    [common]
    {
        screen_name = "Gremlin";
        membership = monster;
    }
    [mind]
    {
        jat_fidget = world\ai\jobs\common\job_fidget.skrit
			?curious				= 0.8
			&wander					= 0.2
			&still					= false
			&meander				= true;
        jat_brain = world\ai\jobs\common\brain_hero.skrit;

        com_range = 11.0;
        melee_engage_range = 12.0;
        ranged_engage_range = 12.0;
        sight_range = 14.0;
        personal_space_range = 0.4;
    }
}

// Instance Template
[t:template,n:doh_gremlin]
{
    category_name = "1W_evil_a";
    doc = "doh_gremlin";
    specializes = base_doh_gremlin;
	[actor]
	{
		[skills]
		{
			strength =  1, 0;
			intelligence =  1, 0;
			dexterity =  1, 0;
			melee = 1, 0;
		}
	}
	[attack]
	{
		attack_range = 1.0;
		damage_max = 1;
		damage_min = 1;
	}
    [inventory]
    {
        spew_equipped_kill_count = <ignore>;
        [equipment]
        {
            es_weapon_hand = dg_mon_002;
        }
    }
}
```
---

Next, multipliers for each monster's stats must be provided in `/scripts/generate_monsters/generate_monsters.csv`.
Base templates do not need to be included in the CSV so long as you're following the input template structures defined above.
Values for each column ***must*** be provided; do not leave any columns blank.

As with the templates themselves, sample values have been provided in the CSV for reference.

---

Once all input templates have been prepared and defined in the CSV, run `dungeon_zig.exe` or `zig build run` and choose option 1 from the menu.
If no errors occurred, output templates will be found in `scripts/generate_monsters/output/`.

---

Finally, if you wish to modify the formulas used during generation, they can be found in `scripts/generate_monsters/GenerateMonstersFormulas.zig`.
Changes to the formulas require re-building the application with `zig build run`.
